﻿App.service('BusService', function () {
    this.emitGetRequest = function ($scope, key, data, done) {
        $scope.$broadcast('GET:' + key + ':' + "OK", data);
        if (!done) {
            $scope.$broadcast('GET:' + key + ':' + "FAIL", data);
        }
    };
    this.emitPostRequest = function ($scope, key, data, done) {
        $scope.$broadcast('POST:' + key + ':' + "OK", data);
        if (!done) {
            $scope.$broadcast('POST:' + key + ':' + "FAIL", data);
        }
    };
    this.emitPutRequest = function ($scope, key, data, done) {
        $scope.$broadcast('PUT:' + key + ':' + "OK", data);
        if (!done) {
            $scope.$broadcast('PUT:' + key + ':' + "FAIL", data);
        }
    };
    this.emitDeleteRequest = function ($scope, key, data, done) {
        $scope.$broadcast('DELETE:' + key + ':' + "OK", data);
        if (!done) {
            $scope.$broadcast('DELETE:' + key + ':' + "FAIL", data);
        }
    };
    this.onGetReceived = function ($scope, key, callback, failback) {
        $scope.$on('GET:' + key + ':OK', function (e, data) { callback(data); });
        if (failback) {
            $scope.$on('GET:' + key + ':FAIL', function (e, data) { failback(data); });
        }
    };
    this.onPostReceived = function ($scope, key, callback, failback) {
        $scope.$on('POST:' + key + ':OK', function (e, data) {
            callback(data);
            $scope.$on('POST:' + key + ':OK', function (e, data) { });
        });
        if (failback) {
            $scope.$on('POST:' + key + ':FAIL', function (e, data) { failback(data); });
        }
    };
    this.onPutReceived = function ($scope, key, callback, failback) {
        $scope.$on('PUT:' + key + ':OK', function (e, data) {
            callback(data);
            $scope.$on('PUT:' + key + ':OK', function (e, data) { });
        });
        if (failback) {
            $scope.$on('PUT:' + key + ':FAIL', function (e, data) { failback(data); });
        }
    };
    this.onDeleteReceived = function ($scope, key, callback, failback) {
        $scope.$on('DELETE:' + key + ':OK', function (e, data) {
            callback(data);
            $scope.$on('DELETE:' + key + ':OK', function (e, data) { });
        });
        if (failback) {
            $scope.$on('DELETE:' + key + ':FAIL', function (e, data) { failback(data); });
        }
    };
});