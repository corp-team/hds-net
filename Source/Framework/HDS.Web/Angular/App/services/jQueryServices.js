﻿App.service('jQueryService', ['$state', function ($state) {
    function initJQueryServices() {
        $.fn.changed = function (callback) {
            $(this).unbind('DOMNodeInserted').bind('DOMNodeInserted', function (event) {
                callback($(event.target));
            });
            return $(this);
        };
        $.fn.announce = function (content, done, doneback) {
            $(this).find('.modal-title:first').html(done ? 'Başarılı İşlem' : 'Başarısız İşlem');
            $(this).find('.modal-body:first').html(content + (done ? '' : ' Lütfen Yeniden Deneyiniz'));
            if (!done) {
                $(this).find('.modal-title:first').css({ color: 'red' });
                $(this).find('.modal-body:first').removeClass("ui success message").addClass('ui error message');
                $(this).find('.announce-close').removeClass('primary').addClass('negative');
            } else {
                $(this).find('.modal-title:first').css({ color: 'green' });
                $(this).find('.modal-body:first').removeClass("ui error message").addClass('ui success message');
                $(this).find('.announce-close').removeClass('negative').addClass('primary');
            }
            $(this).unbind('hidden.bs.modal').on('hidden.bs.modal', function () {
                doneback && doneback();
            });
            $(this).modal({
                closable: true
            }).modal('show');
        };
        $.fn.ask = function (content, approved, cancelled) {
            $(this).find('.modal-title:first').html('Emin misiniz?');
            $(this).find('.modal-body:first').html(content);
            $(this).find('.ask-approved:first').unbind('click').click(approved);
            $(this).find('.ask-cancelled:first').unbind('click').click(cancelled);
            $(this).modal({
                closable: true
            }).modal('show');
        };
    }
    initJQueryServices();
}]);