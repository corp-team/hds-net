﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Objects;
using System;

namespace HDS.App.Extensions.Decorators {

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class ViewBinderAttribute : Attribute {

        public ViewBinderAttribute(Type boundType, string boundProperty) {
            if (typeof(IAGBase).IsAssignableFrom(boundType)) {
                this.BoundType = boundType;
                this.BoundProperty = boundProperty;
            } else {
                throw new ArgumentException($"{boundType} is not a DOBase");
            }
        }

        public Type BoundType { get; private set; }
        public string BoundProperty { get; private set; }
    }

}
