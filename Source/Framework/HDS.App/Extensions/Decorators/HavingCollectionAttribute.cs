﻿using System;

namespace HDS.App.Extensions.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class HavingCollectionAttribute : Attribute {

        public HavingCollectionAttribute(string inverseProperty) {
            this.InverseProperty = inverseProperty;
        }

        public string InverseProperty { get; private set; }

    }
}