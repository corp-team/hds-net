﻿namespace HDS.App.Extensions.Decorators {
    public enum EComputedType {

        Enum,
        FormattedString
    }
}
