﻿using System;
using System.Threading;

namespace HDS.App.Extensions.Static {

    public static class DateTimeExtensions {

        public static string GetTranslatedDateString(this DateTime self, string format) {
            return self.ToString(format, Thread.CurrentThread.CurrentUICulture);

        }

    }

}
