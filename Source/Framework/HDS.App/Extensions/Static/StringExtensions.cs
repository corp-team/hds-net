﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace HDS.App.Extensions.Static {

    public static class StringExtensions {

        public static IEnumerable<string> SlicePartials(this string self, char begin, char[] end) {
            var list = new List<string>();
            for (int i = 0; i < self.Length; i++) {
                if (self[i] == begin) {
                    for (int j = i; j < self.Length; j++) {
                        if (end.Any(c => c == self[j])) {
                            list.Add(self.Substring(i + 1, j - i - 1));
                            break;
                        }
                    }
                }
            }
            return list.ToArray();
        }

        public static string Splice(this string str, char mark, int startIndex = 0) {
            var i = str.IndexOf(mark, startIndex);
            return i > -1 ? str.Substring(0, i) : str;
        }

        public static string Puts(this string self, params object[] args) {

            return String.Format(self, args);

        }

        public static string RandomWords(this int self) {
            var lorem = new Bogus.DataSets.Lorem(locale: "tr");
            return lorem.Words(self).Aggregate("", (acc, n) => $"{acc} {n}");
        }

        public static string GenerateToken(this int count) {

            var str = new StringBuilder();
            for (int i = 0; i < count; i++) {
                str.Append(Guid.NewGuid().ToString("N"));
            }
            return str.ToString();

        }

        public static string RandomDigits(this int count) {

            var random = new Bogus.Randomizer();
            var str = new StringBuilder(random.Number(1, 9).ToString());
            for (int i = 1; i < count; i++) {
                str.Append(random.Number(0, 9).ToString());
            }
            return str.ToString();

        }

        public static string GenerateMD5(this string self) {

            using (var md5 = MD5.Create()) {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(self);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++) {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }

        }

        public static string StripTrailing(this string self, string suffix) {

            var that = self;
            foreach (var c in suffix.Reversed()) {
                that = that.TrimEnd(c);
            }
            return that;
        }

        public static string Reversed(this string self) {
            var ca = self.ToCharArray();
            Array.Reverse(ca);
            return new String(ca);
        }

    }

}
