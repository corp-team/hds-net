﻿using HDS.App.Api.Common;
using HDS.App.Api.Decorators;
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Data;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;

namespace HDS.App.Api.Static {
    public static class SAPIValues {

        public static string CurrentRPIKey { get; set; } = "api";
        public static string CurrentRPIVersion { get; set; } = "v1";

        private static string currentDebugHost;
        private static string currentProductionHost;

        public static string CurrentDebugHost {
            private get => String.IsNullOrEmpty(currentDebugHost) ? throw new Exception("Current Debug Host Not Set") : currentDebugHost;
            set => currentDebugHost = value;
        }
        public static string CurrentProductionHost {
            private get => String.IsNullOrEmpty(currentProductionHost) ? throw new Exception("Current Production Host Not Set") : currentProductionHost;
            set => currentProductionHost = value;
        }

        public static string CurrentRemoteHost {
            get {
                return $"{(SDbParams.LocalMachine() ? CurrentDebugHost : CurrentProductionHost) }";
            }
        }

        public static IEnumerable<Type> ReferencedDomains(Type aggregateType) {
            var domains = aggregateType.Assembly.GetTypes().Where(t => !t.IsGenericType && t.HasInterface<IDOBase>()).ToList();
            var aggregates = aggregateType.Assembly.GetTypes().Where(t => !t.IsGenericType && t.HasInterface<IAGBase>()).ToList();
            return domains.Where(d => !Activator.CreateInstance(aggregateType).As<IAGBase>().MainDomain.Equals(d)).ToArray();
        }

        public static IEnumerable<ApiMetadata> LoadTypedMetadata<TApplication, TTemplateEntity>()
            where TApplication : HttpApplication
            where TTemplateEntity : DOBase<TTemplateEntity> {
            
            var typedControllers = typeof(TApplication).Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(GenericController)) && t.Equals(typeof(GenericController)) == false).ToList();

            foreach (var hqctrl in typedControllers) {
                var apiConfigAttr = hqctrl.GetCustomAttribute<ApiConfigAttribute>();
                var hqactions = hqctrl.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => m.GetCustomAttributes().Any(ca => ca.GetType().IsSubclassOf(typeof(HQRouteAttribute))));
                var methodAttrTuple = hqactions.Select(hqa => 
                    (method: hqa, attr: hqa.GetCustomAttributes().SingleOrDefault(ca => ca.GetType().IsSubclassOf(typeof(HQRouteAttribute))) as HQRouteAttribute)).ToList();
                yield return new ApiMetadata { ApiConfig = apiConfigAttr, RouteInfos = methodAttrTuple.ToArray() };
            }

        }

        public static IEnumerable<ApiMetadata> LoadDynamicMetadata<TTemplateEntity>()
            where TTemplateEntity : DOBase<TTemplateEntity> {

            var aggregates = typeof(TTemplateEntity).Assembly.GetTypes().Where(t => !t.IsGenericType && t.HasInterface<IAGBase>()).ToList();
            foreach (var aggregate in aggregates) {
                var hqactions = typeof(GenericController).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                   .Where(m => m.GetCustomAttributes().Any(ca => ca.GetType().IsSubclassOf(typeof(HQRouteAttribute))));
                var methodAttrTuple = hqactions.Select(hqa =>
                    (method: hqa, attr: hqa.GetCustomAttributes().SingleOrDefault(ca => ca.GetType().IsSubclassOf(typeof(HQRouteAttribute))) as HQRouteAttribute)).ToList();
                yield return new ApiMetadata {
                    ApiConfig = new ApiConfigAttribute(Activator.CreateInstance(aggregate).As<IAGBase>().ResourceName, typeof(GenericController), aggregate),
                    RouteInfos = methodAttrTuple.ToArray()
                };
            }

        }

        internal static string CurrentRemoteAddress {
            get {
                return $"{CurrentRemoteHost}/{CurrentRPIKey}/{CurrentRPIVersion}";
            }
        }

    }
}