﻿using HDS.App.Api.Contracts;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Contracts;
using HDS.App.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace HDS.App.Api.Membership.Decorators {
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    internal sealed class TahditliAttribute : ActionFilterAttribute {

        private readonly static IDOEngine<Aza> _AzaEngine = SDataEngine.GenerateDOEngine<Aza>();

        public override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {
            var controllerType = actionExecutedContext.ActionContext.ControllerContext.Controller.GetType();
            var actionName = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;
            if (controllerType.GetMethod(actionName).GetCustomAttribute<MusaadeliAttribute>() != null) {
                return OKResponse(actionExecutedContext, "Permitted");
            } else if (actionExecutedContext.Request.Headers.TryGetValues("X-HDS-Tahdit", out var headers)) {
                if (!Celse.Cari(headers.FirstOrDefault(), out var aza)) {
                    return FaultResponse(actionExecutedContext, "Invalid Token");
                } else {
                    var kaideAttrs = controllerType.GetMethod(actionName).GetCustomAttributes<KaideAttribute>();
                    var vazifeAttr = controllerType.GetMethod(actionName).GetCustomAttribute<VazifeAttribute>();
                    if (kaideAttrs.Count() > 0) {
                        var vresp = SDataEngine.GenerateDOEngine<Vazife>().SelectSingleSync(x => x.Tarif == vazifeAttr.Tarif);
                        if (vresp.ContainsItem) {
                            foreach (var kaideTarif in kaideAttrs.Select(ka => ka.Tarif)) {
                                var kresp = SDataEngine.GenerateDOEngine<Kaide>().SelectSingleSync(x => x.Tarif == kaideTarif);
                                if (kresp.ContainsItem && vresp.ContainsItem) {
                                    var k = kresp.SingleResponse;
                                    var v = vresp.SingleResponse;
                                    var tahditlerResp = SDataEngine.GenerateDOEngine<Tahdit>()
                                        .SelectBySync(t => t.KaideID == k.ID && t.VazifeID == v.ID);
                                    if (tahditlerResp.ContainsItem) {
                                        var tahditler = tahditlerResp.CollectionResponse;
                                        if (!tahditler.Select(t => t.AzaID).Contains(aza.ID)) {
                                            return FaultResponse(actionExecutedContext, $"Rule Violation, {aza} Not Permitted For Role: {v} To {k}");
                                        }
                                    } else {
                                        return FaultResponse(actionExecutedContext, $"Server Error, {tahditlerResp.Message}");
                                    }
                                } else {
                                    return FaultResponse(actionExecutedContext, $"Server Error, {kresp.Message}, {vresp.Message}");
                                }
                            }
                        } else {
                            return FaultResponse(actionExecutedContext, $"Server Error, {vresp.Message}");
                        }
                    }
                    if (DateTime.Now > aza.AccessTokenExpiresOn) {
                        var token = Guid.NewGuid().ToString("N");
                        _AzaEngine.UpdateSync(aza.ID, (modifier) => modifier
                           .Update(a => a.AccessToken).Set(token)
                           .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    }
                    return OKResponse(actionExecutedContext, "Token Refreshed");
                }
            } else {
                return FaultResponse(actionExecutedContext, "No Token Provided");
            }
        }

        private Task<HttpResponseMessage> FaultResponse(HttpActionExecutedContext actionContext, string message) {
            actionContext.Response = new HttpResponseMessage {
                StatusCode = HttpStatusCode.Forbidden,
                RequestMessage = actionContext.Request,
                ReasonPhrase = message
            };
            return Task.FromResult(actionContext.Response);
        }

        private Task<HttpResponseMessage> OKResponse(HttpActionExecutedContext actionContext, string message) {
            actionContext.Response.ReasonPhrase = message;
            return Task.FromResult(actionContext.Response);
        }
    }
}