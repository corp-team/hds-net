﻿
using HDS.App.Extensions.Decorators;

namespace HDS.App.Api.Membership.Models {

    [ResourceLocator(typeof(Resources.Meslek))]
    public enum EMeslek {

        Tuccar = 1,
        Hekim,
        Alim,
        EvHanimi

    }

}
