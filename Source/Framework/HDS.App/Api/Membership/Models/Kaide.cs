﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace HDS.App.Api.Membership.Models {
    public class Kaide : MembershipDOBase<Kaide> {

        public string Tarif { get; set; }

        public string Unsur { get; set; }

        public override string TextValue => Tarif;

        protected override void EMMap(IDOTableBuilder<Kaide> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Kaideler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            builder.For(d => d.Unsur).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(512);

            builder.UniqueKey(x => x.Tarif);
        }
    }
}
