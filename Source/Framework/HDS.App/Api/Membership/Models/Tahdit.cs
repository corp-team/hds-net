﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace HDS.App.Api.Membership.Models {
    public class Tahdit : MembershipDOBase<Tahdit> {


        public long AzaID { get; set; }

        public long KaideID { get; set; }

        public long VazifeID { get; set; }

        public string Tarif { get; set; }

        public override string TextValue => Tarif;

        protected override void EMMap(IDOTableBuilder<Tahdit> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Tahditler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

            builder.ForeignKey(a => a.AzaID).References<Aza>(a => a.ID);
            builder.ForeignKey(a => a.KaideID).References<Kaide>(a => a.ID);
            builder.ForeignKey(a => a.VazifeID).References<Vazife>(a => a.ID);

            builder.UniqueKey(a => a.AzaID, a => a.KaideID, a => a.VazifeID);
            builder.UniqueKey(a => a.Tarif);

        }
    }
}
