﻿using HDS.App.Api.Membership.Models;
using HDS.App.Api.Membership.Views.Resources;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace HDS.App.Api.Membership.Views {

    [ResourceLocator(typeof(AzaResource))]
    public class AzaView : AGBase<AzaView> {

        [IDPropertyLocator]
        public long AzaID { get; set; }

        public long AzaKunyeID { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeIsim { get; set; }

        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeSoyisim { get; set; }

        [TableColumn(Order = 6), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeMemleket { get; set; }

        [TableColumn(Order = 7), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeIrtibatTelefonu { get; set; }

        public DateTime KunyeMevlid { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(DateTime))]
        [TableColumn(Order = 8), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeMevlidComputed {
            get => KunyeMevlid.GetTranslatedDateString(format: "dd MMMM yyyy");
            set => KunyeMevlid = DateTime.Parse(value, CultureInfo.DefaultThreadCurrentUICulture);
        }

        public EMeslek KunyeMeslek { get; set; }
        [ComputedField(EComputedType.Enum, typeof(EMeslek)), FormField(EFormType.Insert | EFormType.Edit)]
        [TableColumn(Order = 9)]
        public string KunyeMeslekComputed {
            get => KunyeMeslek != 0 ? KunyeMeslek.GetEnumResource() : "";
            set => KunyeMeslek = value.ToEnum<EMeslek>();
        }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public long AzaTCKN { get; set; }

        [TableColumn(Order = 4, Disabled = true, InputType = "password"), FormField(EFormType.Insert)]
        public string AzaSifre { get; set; }

        public string AzaAccessToken { get; set; }

        public string AzaActivationToken { get; set; }

        public string AzaRecoveryToken { get; set; }

        public DateTime AzaActivationTokenExpiresOn { get; set; }

        public DateTime AzaAccessTokenExpiresOn { get; set; }

        public DateTime AzaRecoveryTokenExpiresOn { get; set; }

        [HavingCollection("TahditAzaID")]
        [FormField(EFormType.Insert | EFormType.Edit)]
        public List<TahditView> Tahditler { get; set; } = new List<TahditView>();

        public override Type MainDomain => typeof(Aza);

        public override Type[] ReferencedDomains => new[] { typeof(Kunye) };

        public override string TextValue => KunyeIsim;

        public override string ResourceName => "aza";

        protected override void Map(IAGViewBuilder<AzaView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("AzaView"))
                .Select<Aza>(li =>
                    li
                    .Map(x => x.ID, x => x.AzaID)
                    .Map(x => x.KunyeID, x => x.AzaKunyeID)
                    .Map(x => x.TCKN, x => x.AzaTCKN)
                    .Map(x => x.Sifre, x => x.AzaSifre)
                    .Map(x => x.AccessToken, x => x.AzaAccessToken)
                    .Map(x => x.ActivationToken, x => x.AzaActivationToken)
                    .Map(x => x.RecoveryToken, x => x.AzaRecoveryToken)
                    .Map(x => x.AccessTokenExpiresOn, x => x.AzaAccessTokenExpiresOn)
                    .Map(x => x.ActivationTokenExpiresOn, x => x.AzaActivationTokenExpiresOn)
                    .Map(x => x.RecoveryTokenExpiresOn, x => x.AzaRecoveryTokenExpiresOn)
                ).OuterJoin<Kunye>(li =>
                    li
                    .Map(x => x.IrtibatTelefonu, x => x.KunyeIrtibatTelefonu)
                    .Map(x => x.Isim, x => x.KunyeIsim)
                    .Map(x => x.Soyisim, x => x.KunyeSoyisim)
                    .Map(x => x.Memleket, x => x.KunyeMemleket)
                    .Map(x => x.Meslek, x => x.KunyeMeslek)
                    .Map(x => x.Mevlid, x => x.KunyeMevlid)
                ).On<Aza>((a, b) => a.ID == b.KunyeID);

        }

    }

}
