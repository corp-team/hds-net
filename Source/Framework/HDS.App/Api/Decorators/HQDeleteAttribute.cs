﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Decorators {
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class HQDeleteAttribute : HQRouteAttribute {
        public HQDeleteAttribute(string path) : base(path) {
        }

        public override HttpMethod Method => HttpMethod.Delete;
    }
}
