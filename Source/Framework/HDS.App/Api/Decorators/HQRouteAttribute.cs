﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Decorators {
    public abstract class HQRouteAttribute : Attribute {

        readonly string path;

        public HQRouteAttribute(string path) {
            this.path = path;
        }

        public string Path {
            get { return path; }
        }

        public Type ArgumentType { get; set; }
        public abstract HttpMethod Method { get; }
    }
}
