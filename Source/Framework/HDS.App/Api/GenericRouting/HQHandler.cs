﻿using HDS.App.Api.Common;
using HDS.App.Api.Decorators;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Api.Static;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Static;
using HDS.App.Api.APIExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using System.Web.Routing;
using System.Web.Http.Routing;
using System.Web.Http.Hosting;

namespace HDS.App.Api.GenericRouting {

    public class HQHandler : DelegatingHandler {

        private HttpConfiguration config;
        private IEnumerable<ApiMetadata> metaDatas;
        private IEnumerable<(MethodInfo method, HQRouteAttribute attr)> infos;

        public HQHandler(HttpConfiguration config, IEnumerable<ApiMetadata> metaDatas, IEnumerable<(MethodInfo method, HQRouteAttribute attr)> infos) {
            this.config = config;
            this.metaDatas = metaDatas;
            this.infos = infos;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
            return await ExecuteAction(request, cancellationToken);
        }

        private async Task<HttpResponseMessage> ExecuteAction(HttpRequestMessage request, CancellationToken cancellationToken) {
            var routeData = request.GetRequestContext().RouteData;
            var key = routeData.Values["controller"].ToString();
            var metaData = this.metaDatas.Single(m => m.ApiConfig.Key == key);
            var controller = metaData.ApiConfig.Controller;
            var aggregate = metaData.ApiConfig.Aggregate;

            object apiController = new GenericController(aggregate);
            if (!(controller is null) && controller.Equals(typeof(GenericController)) == false) {
                apiController = Activator.CreateInstance(controller);
            }
            return await ExecuteDirect(request, aggregate, routeData, apiController as ApiController, metaData, cancellationToken);
        }

        private async Task<HttpResponseMessage> ExecuteDirect(HttpRequestMessage request, 
            Type aggregate, IHttpRouteData routeData, ApiController apiController, ApiMetadata metaData, CancellationToken cancellationToken) {
            Task<IHttpActionResult> task = null;
            var action = routeData.Values["action"].ToString();
            var info = this.infos.SingleOrDefault(a => action.Equals(a.attr.Path) || action.Equals(a.attr.Path.Splice('/')));

            if (info.attr.ArgumentType is null) {
                task = apiController.ExecuteMethod(info.method.Name, Type.EmptyTypes) as Task<IHttpActionResult>;
            } else {
                var argType = info.attr.ArgumentType.Equals(typeof(IAGBase)) ? aggregate : info.attr.ArgumentType;
                object[] arguments = null;
                if (info.attr.Method == HttpMethod.Get) {
                    if (info.attr.ArgumentType.Equals(typeof(Dictionary<string, object>))) {
                        var dict = new Dictionary<string, object>();
                        var coll = HttpUtility.ParseQueryString(request.RequestUri.Query);
                        foreach (string key in coll.Keys) {
                            dict.Add(key, coll[key]);
                        }
                        arguments = new object[] { dict };
                    } else {
                        arguments = info.attr.Path.SlicePartials(begin: '{', end: new char[] { '}', ':' })
                            .Select(arg => Convert.ChangeType(routeData.Values[arg], info.attr.ArgumentType)).ToArray();
                    }
                } else if (info.attr.ArgumentType.Equals(typeof(long))) {
                    var uri = request.RequestUri.ToString();
                    arguments = new object[] { long.Parse(uri.Substring(uri.LastIndexOf('/') + 1)) };
                } else {
                    arguments = new object[] { (await request.Content.ReadAsStringAsync()).ToObject(argType) };
                }
                task = apiController.ExecuteMethod(info.method.Name, Type.EmptyTypes, arguments) as Task<IHttpActionResult>;
            }
            var result = await task;
            var executed = await result.ExecuteAsync(cancellationToken);

            var ctrDesc = new HttpControllerDescriptor(config, metaData.ApiConfig.Key, metaData.ApiConfig.Controller);
            var actDesc = new ReflectedHttpActionDescriptor(ctrDesc, info.method);
            var actionExecutedContext = new HttpActionExecutedContext {
                ActionContext = new HttpActionContext {
                    ControllerContext = new HttpControllerContext {
                        Request = request,
                        Controller = apiController
                    },
                    ActionDescriptor = actDesc
                },
                Response = executed
            };
            foreach (ActionFilterAttribute filter in info.method.GetCustomAttributes(typeof(ActionFilterAttribute), true)) {
                await filter.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
            }
            return actionExecutedContext.Response;
        }

    }

}
