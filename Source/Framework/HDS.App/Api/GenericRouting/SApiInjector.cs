﻿using HDS.App.Api.ActionFilters;
using HDS.App.Api.APIExtensions;
using HDS.App.Api.Common;
using HDS.App.Api.Decorators;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Api.Membership.Models;
using HDS.App.Api.Static;
using HDS.App.Domain.Objects;
using System;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Routing;

namespace HDS.App.Api.GenericRouting {
    public static class SApiInjector {

        public static void InjectApi<TApplication, TTemplateEntity>(HttpConfiguration config)
            where TApplication : HttpApplication
            where TTemplateEntity : DOBase<TTemplateEntity> {

            var mdatas = SAPIValues.LoadTypedMetadata<TApplication, Aza>().Union(SAPIValues.LoadDynamicMetadata<TTemplateEntity>());
            foreach (var metadata in mdatas) {
                foreach (var info in metadata.RouteInfos) {
                    config.Routes.MapHttpRoute(
                        name: $"ApiRoute-{Guid.NewGuid().ToString("N")}",
                        routeTemplate: $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/{{controller}}/{{action}}/{{argument}}",
                        defaults: new { controller = metadata.ApiConfig.Key, action = info.attr.Path, argument = RouteParameter.Optional },
                        handler: new HQHandler(config, mdatas, metadata.RouteInfos),
                        constraints: new {
                            httpMethod = new HttpMethodConstraint(info.attr.Method)
                        }
                    );
                }
            }
            
            config.MapHttpAttributeRoutes();
            config.Filters.Add(new TahditliAttribute());
            config.Filters.Add(new VazifeAttribute());
            config.Filters.Add(new KaideAttribute());

        }

    }
}
