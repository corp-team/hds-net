﻿using HDS.App.Api.ActionFilters;
using HDS.App.Api.Contracts;
using HDS.App.Api.Decorators;
using HDS.App.Api.GenericRouting;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Api.Static;
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Domain.Objects;
using HDS.App.Engines;
using HDS.App.Engines.Core;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using HDS.App.Internalization.Strings;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace HDS.App.Api.Common {

    public class GenericController : ApiController, IApiController {

        #region Ctor

        public GenericController(Type aggregateType) : base() {

            DomainType = Activator.CreateInstance(aggregateType).As<IAGBase>().MainDomain;
            AggregateType = aggregateType;
            Request = new HttpRequestMessage();

        }

        #endregion

        #region Properties

        private Type DomainType { get; set; }
        private Type AggregateType { get; set; }

        public string ListRoute => "list";

        public static Dictionary<EApiRoutes, string> Routes(Type domainType) {
            return new Dictionary<EApiRoutes, string>() {
                { EApiRoutes.All, "all" },
                { EApiRoutes.Columns, "columns" },
                { EApiRoutes.Fields, "fields/{argument:long}" },
                { EApiRoutes.List, "list" },
                { EApiRoutes.One, "one/{argument:long}" },
                { EApiRoutes.Translations, "translations" },
                { EApiRoutes.Insert, "insert" },
                { EApiRoutes.Edit, "edit" },
                { EApiRoutes.Delete, "delete" },
            }.Select(pair => new KeyValuePair<EApiRoutes, string>(pair.Key,
                $"{SAPIValues.CurrentRemoteAddress}/{domainType.Name.ToLower()}/{pair.Value}")).ToDictionary(p => p.Key, p => p.Value);
        }

        #endregion

        #region Get Methods
        [HQGet(path: "all", ArgumentType = typeof(Dictionary<string, object>))]
        [Tahditli, Vazife("Aza"), Kaide("lists")]
        public async Task<IHttpActionResult> All([FromUri] Dictionary<string, object> query) {
            var eng = GenerateAGEngine();
            var listResponse = await eng.GenericSelectAll();
            var count = listResponse.DynamicCollectionResponse.Count();
            var ranges = "";
            if (query != null) {
                //sort
                if (query.ContainsKey("sort")) {
                    var sort = query["sort"].ToString().ToObject<string[]>();
                    for (int i = 0; i < sort.Length; i += 2) {
                        var propName = sort[i];
                        var order = sort[i + 1];
                        if (order == "DESC") {
                            listResponse.DynamicCollectionResponse.OrderByDescending(item => item.GetType().GetProperty(propName).GetValue(item));
                        } else {
                            listResponse.DynamicCollectionResponse.OrderBy(item => item.GetType().GetProperty(propName).GetValue(item));
                        }
                    }
                }
                //range
                if (query.ContainsKey("range")) {
                    var range = query["range"].ToString().ToObject<int[]>();
                    if (range[1] == -1) {
                        ranges = $"{range[0]}-{count}/{count}";
                    } else {
                        listResponse.DynamicCollectionResponse =
                            listResponse.DynamicCollectionResponse.Skip(range[0]).Take(range[1] - range[0]).ToArray();
                    }
                    ranges = $"{range[0]}-{range[1]}/{count}";
                }
                //filter
                if (query.ContainsKey("filter")) {
                    var filter = query["filter"].ToString().ToObject<Dictionary<string, object>>();
                    foreach (var entry in filter) {
                        listResponse.DynamicCollectionResponse = listResponse.DynamicCollectionResponse
                            .Where(item => item.GetType().GetProperty(entry.Key).GetValue(item).ToString().Contains(entry.Value.ToString()));
                    }
                }
            }

            return JsonWithHeader(listResponse, (h) => h.Add("X-Content-Range", ranges));
        }

        [HQGet(path: "grid-fields")]
        [Tahditli, Vazife("Aza"), Kaide("lists")]
        public async Task<IHttpActionResult> GridFields() {
            var dict = PickFields();
            return await Task.FromResult(JsonWithHeader(dict));
        }

        [HQGet(path: "one/{argument:int:min(1)}", ArgumentType = typeof(long))]
        public async Task<IHttpActionResult> One(long id) {
            var eng = GenerateAGEngine();
            return JsonWithHeader(await eng.GenericSelectByID(id));
        }

        [HQGet(path: "list")]
        public async Task<IHttpActionResult> List() {
            var eng = GenerateDOEngine();
            var response = await eng.GenericSelectAll();
            if (response.ContainsItem) {
                var list = response.DynamicCollectionResponse.Select(q =>
                    new { id = q.ID, text = q.TextValue }).ToList();
                list.Insert(0, new { id = (dynamic)0L, text = (dynamic)Preformats.FieldPlaceholder.Puts(AggregateType.GetStringResource("TitleEntity")) });
                return JsonWithHeader(list.AsEnumerable());
            } else {
                return JsonWithHeader(response);
            }
        }

        [HQGet(path: "translations")]
        public async Task<IHttpActionResult> Translations() {
            var rm = new ResourceManager(AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            var set = rm.GetResourceSet(Thread.CurrentThread.CurrentCulture, true, true);
            var dict = new Dictionary<string, string>();
            foreach (DictionaryEntry it in set) {
                string key = it.Key.ToString();
                string resource = it.Value.ToString();
                dict[key] = resource;
            }
            return await Task.FromResult(JsonWithHeader(dict));
        }

        [HQGet(path: "columns")]
        public async Task<IHttpActionResult> Columns() {
            var list = PickColumns(pi => pi.HasAttribute<TableColumnAttribute>() &&
                pi.GetCustomAttribute<TableColumnAttribute>().Disabled == false).ToList();
            return await Task.FromResult(JsonWithHeader(list));
        }

        [HQGet(path: "fields/{argument:int:min(1)}", ArgumentType = typeof(long))]
        public async Task<IHttpActionResult> Fields(long formType) {
            var type = (EFormType)formType;
            var list = PickColumns((pi) => (pi.GetCustomAttribute<FormFieldAttribute>()?.FormType & type) == type
                || pi.HasAttribute<HavingCollectionAttribute>()).ToList();
            return await Task.FromResult(JsonWithHeader(list));
        }

        #endregion

        #region Post Methods
        [HQPost(path: "insert-domain")]
        public async Task<IHttpActionResult> Insert([FromBody]IDOBase domain) {

            return await Task.FromResult(JsonWithHeader(domain.InsertSelf()));

        }

        [HQPost(path: "insert")]
        public async Task<IHttpActionResult> InsertAggregate([FromBody]IAGBase aggregate) {
            return await Task.FromResult(JsonWithHeader(aggregate.InsertSelf()));
        }

        [HQPut(path: "edit")]
        public async virtual Task<IHttpActionResult> Edit([FromBody]IAGBase aggregate) {

            var eng = GenerateDOEngine();
            return JsonWithHeader(await eng.GenericUpdateAggregate(aggregate));

        }

        [HQDelete(path: "delete/{argument}", ArgumentType = typeof(long))]
        public async virtual Task<IHttpActionResult> Delete(long id) {
            var eng = GenerateAGEngine();
            var aggregateResp = await eng.GenericSelectByID(id);
            if (aggregateResp.ContainsItem == false) {
                return JsonWithHeader(aggregateResp);
            }
            var aggregate = aggregateResp.DynamicSingleResponse as IAGBase;
            return JsonWithHeader(aggregate.DeleteSelf());
        }

        #endregion

        #region Special Methods

        private IDictionary<string, object> PickFields() {
            var dict = new Dictionary<string, object>();
            var resourceType = AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in AggregateType.GetProperties()
               .Where(p => p.HasAttribute<TableColumnAttribute>())
               .OrderBy(prop => prop.GetCustomAttribute<TableColumnAttribute>().Order)) {
                var type = GetEditor(col, out var editor, out var inputType);
                dict[col.Name] =
                    new {
                        name = col.Name,
                        inputType = inputType,
                        title = $"{AggregateType.GetStringResource(col.Name)}",
                        type = type,
                        editor = editor,
                        maxLength = (Activator.CreateInstance(AggregateType) as IAGBase).GetMaxLength(col),
                        required = (Activator.CreateInstance(AggregateType) as IAGBase).IsColumnRequired(col),
                        placeholder = Preformats.FieldPlaceholder.Puts(AggregateType.GetStringResource(col.Name))
                    };
            }
            return dict;
        }

        private string GetEditor(PropertyInfo col, out IDictionary<string, object> dict, out string inputType) {
            var type = "string";
            inputType = "text";
            dynamic editor = new {
                type = type
            };
            if (col.PropertyType.OfType<DateTime>()) {
                type = "datetime";
                editor = new {
                    type = "datepicker",
                    component = "",
                };
            } else if (col.PropertyType.OfType<bool>()) {
                editor = new {
                    type = "checkbox",
                    config = new {
                        @true = "Var",
                        @false = "Yok"
                    }
                };
                inputType = "radio";
            } else if (col.PropertyType.IsNumericType()) {
                editor = new {
                    type = "number"
                };
                type = "number";
                inputType = "number";
            } else if (col.HasAttribute<HavingCollectionAttribute>() && col.PropertyType.IsCollection()) {
                var aggragateType = col.PropertyType.GetGenericArguments().First();
                var domain = Activator.CreateInstance(aggragateType).As<IAGBase>().MainDomain;
                var eng = Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(aggragateType)) as IGenericEngine;
                var list = eng.GenericSelectAllSync();
                editor = new {
                    type = "list",
                    config = new {
                        list = list.DynamicCollectionResponse.Select(item => new { value = item.IDProperty, title = item.TextValue })
                    }
                };
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    var list = Enum.GetValues(attr.ReferencedType).OfType<object>().Select(val =>
                        new { value = Enum.GetName(attr.ReferencedType, val), title = val.GetEnumResource(attr.ReferencedType) });
                    editor = new {
                        type = "list",
                        config = new {
                            list = list
                        }
                    };
                } else if (attr.ComputedType == EComputedType.FormattedString) {
                    if (attr.ReferencedType.Equals(typeof(DateTime))) {
                        type = "datetime";
                        editor = new {
                            type = "datepicker",
                            component = "",
                        };
                    }
                }
            }
            dynamic exp = new ExpandoObject();
            dict = exp;
            if (col.HasAttribute<TableColumnAttribute>(out var tcol)) {
                if (tcol.Order == 0) {
                    dict.Add("editable", false);
                }
                if (tcol.Disabled == true) {
                    inputType = "password";
                }
            }
            foreach (PropertyInfo prop in editor.GetType().GetProperties()) {
                dict[prop.Name] = prop.GetValue(editor);
            }
            return type;
        }

        private IEnumerable<IDictionary<string, object>> PickColumns(Func<PropertyInfo, bool> filter = null) {

            var resourceType = AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in AggregateType.GetProperties()
                .Where(p => p.HasAttribute<TableColumnAttribute>())
                .Where(p => filter?.Invoke(p) ?? true)
                .OrderBy(prop => prop.GetCustomAttribute<TableColumnAttribute>().Order)) {
                var dataType = GetDataTypeOfEditable(col, out var inputType, out var selectables, out var link);
                var dict = new Dictionary<string, object> {
                    { "name", col.Name },
                    { "defaultValue", null },
                    { "isRequired", (Activator.CreateInstance(AggregateType) as IAGBase).IsColumnRequired(col) },
                    {
                        "formdata",
                        new {
                            placeholder = Preformats.FieldPlaceholder.Puts(AggregateType.GetStringResource(col.Name)),
                            title = $"{AggregateType.GetStringResource(col.Name)}:",
                            isNavigationProperty = false,
                            dataType = dataType,
                            inputType = inputType,
                            link = dataType == "collection" ? link : null,
                            selectables = dataType == "selectable" ? selectables : null
                        }
                    }
                };
                if (filter == null) {
                    dict.Add("label", rm?.GetString(col.Name, Thread.CurrentThread.CurrentCulture) ?? col.Name);
                } else if (filter(col)) {
                    dict.Add("label", rm?.GetString(col.Name, Thread.CurrentThread.CurrentCulture) ?? col.Name);
                } else {
                    continue;
                }
                yield return dict;
            }
        }

        private JsonWithHeader<T> JsonWithHeader<T>(T listResponse, Action<HttpHeaders> headerer = null) {
            var resp = new JsonWithHeader<T>(headerer, listResponse, new JsonSerializerSettings {
                ContractResolver = TableColumnContractResolver.Instance
            }, Encoding.UTF8, Request);
            return resp;
        }

        private static string GetDataTypeOfEditable(PropertyInfo col, out string inputType, out List<object> selectables, out string link) {
            selectables = new List<object>();
            link = "";
            var dataType = "string";
            inputType = "";
            if (col.PropertyType.OfType<DateTime>()) {
                dataType = "date";
            } else if (col.PropertyType.OfType<bool>()) {
                dataType = "boolean";
            } else if (col.PropertyType.IsNumericType()) {
                dataType = "number";
            } else if (col.HasAttribute<HavingCollectionAttribute>() && col.PropertyType.IsCollection()) {
                dataType = "collection";
                link = Routes(Activator.CreateInstance(col.PropertyType.GetGenericArguments().First()).As<IAGBase>().MainDomain)[EApiRoutes.List];
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                dataType = "computed";
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    dataType = "selectable";
                    foreach (var val in Enum.GetValues(attr.ReferencedType)) {
                        selectables.Add(new { value = Enum.GetName(attr.ReferencedType, val), text = val.GetEnumResource(attr.ReferencedType) });
                    }
                } else if (attr.ComputedType == EComputedType.FormattedString) {
                    if (attr.ReferencedType.Equals(typeof(DateTime))) {
                        dataType = "datetime";
                    }
                }
            }

            if (dataType == "numeric" || dataType == "text") {
                var colInputType = col.GetCustomAttribute<TableColumnAttribute>().InputType;
                inputType = string.IsNullOrEmpty(colInputType) ? dataType : colInputType;
                dataType = "input";
            }
            return dataType;
        }

        private IGenericEngine GenerateDOEngine() {
            return Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(DomainType)) as IGenericEngine;
        }

        private IGenericEngine GenerateAGEngine() {
            return Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(AggregateType)) as IGenericEngine;
        }

        #endregion

    }

}
