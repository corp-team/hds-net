﻿
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Api.Common {
    internal class TableColumnContractResolver : DefaultContractResolver {
        public static readonly TableColumnContractResolver Instance = new TableColumnContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
            var t = property.DeclaringType;
            if (!t.IsGenericType && t.HasInterface<IAGBase>()) {
                property.ShouldSerialize =
                    instance => {
                        var prop = property.DeclaringType.GetProperty(property.PropertyName);
                        return prop.HasAttribute<TableColumnAttribute>() && prop.GetCustomAttribute<TableColumnAttribute>().Disabled == false;
                    }; 
            }
            return property;
        }
    }
}
