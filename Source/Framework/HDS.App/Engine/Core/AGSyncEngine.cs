﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HDS.App.Engines.Core {
    internal class AGSyncEngine<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        protected Func<Exception, DMLResponse<TAggregate>> Failback { get; private set; }

        public AGSyncEngine() {
            Failback = (exc) => new DMLResponse<TAggregate>() {
                Fault = exc,
                ContainsItem = false
            };
        }
        public DMLResponse<TAggregate> SelectAllSync(Func<TAggregate, bool> filter = null) {
            DMLResponse<TAggregate> response = new DMLResponse<TAggregate>() { ContainsItem = false };
            var list = new List<TAggregate>();
            AG<TAggregate>.Views().SelectAllColumns().ExecuteList((item) => {
                if (filter != null) {
                    if (filter(item)) list.Add(item);
                } else {
                    list.Add(item);
                }
            }, () => response.ContainsItem = false, (ex) => response = Failback(ex)).Wait();
            response = new DMLResponse<TAggregate> { CollectionResponse = list };
            return response;
        }

        public DMLResponse<TAggregate> SelectBySync(params Expression<Func<TAggregate, bool>>[] selectors) {
            DMLResponse<TAggregate> response = new DMLResponse<TAggregate>() { ContainsItem = false };
            var list = new List<TAggregate>();
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement.AndAlso(sel));
            confinement.ExecuteList((item) => {
                list.Add(item);
            }, () => response.ContainsItem = false, (ex) => response = Failback(ex)).Wait();
            response = new DMLResponse<TAggregate> { CollectionResponse = list };
            return response;
        }

        public DMLResponse<TAggregate> SelectByIDSync(long id) {
            DMLResponse<TAggregate> response = new DMLResponse<TAggregate> { ContainsItem = false };
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where((agg) => agg.IDProperty == id);
            confinement.ExecuteSingle((item) => {
                response = new DMLResponse<TAggregate> { SingleResponse = item, ContainsItem = true };
            }, () => response.ContainsItem = false, (ex) => response = Failback(ex)).Wait();
            return response;
        }

        public DMLResponse<TAggregate> SelectSingleSync(Expression<Func<TAggregate, bool>> selector) {
            DMLResponse<TAggregate> response = new DMLResponse<TAggregate>() { ContainsItem = false };
            AG<TAggregate>.Views().SelectAllColumns().Where(selector).ExecuteSingle((item) => {
                response = new DMLResponse<TAggregate> { SingleResponse = item };
            }, () => response.ContainsItem = false, (ex) => response = Failback(ex)).Wait();
            return response;
        }

        public IDMLResponse SelectByComparison<T>(PropertyInfo targetProp, T target) {
            DMLResponse<TAggregate> response = null;
            var li = new List<TAggregate>();
            AG<TAggregate>.Views().SelectAllColumns().ExecuteList((item) => {
                var value = targetProp.GetValue(item);
                if (value.Equals(target)) {
                    li.Add(item);
                }
            }, () => response.ContainsItem = false, (ex) => response = Failback(ex)).Wait();
            response = response ?? new DMLResponse<TAggregate>() { ContainsItem = true, CollectionResponse = li };
            return response;
        }
    }
}
