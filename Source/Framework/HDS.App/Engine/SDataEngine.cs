﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Data;
using HDS.App.Domain.Objects;
using HDS.App.Engines.Core;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HDS.App.Engines {
    public static class SDataEngine {

        public static IDOEngine<TEntity> GenerateDOEngine<TEntity>()
            where TEntity : DOBase<TEntity> {
            return new DOEngine<TEntity>();
        }

        public static IAGEngine<TAggregate> GenerateAGEngine<TAggregate>()
            where TAggregate : AGBase<TAggregate> {
            return new AGEngine<TAggregate>();
        }

        public static IGenericEngine GenerateDOEngine(Type domainType) {
            return Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(domainType)).As<IGenericEngine>();
        }

        public static dynamic GenerateAGEngine(Type aggregateType) {
            return Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(aggregateType));
        }

        public static void BuildDomain(AssemblyName assemblyName, bool drop) {
            GenerateObjects(assemblyName, out var domainObjects, out var aggregateObjects);

            if (drop) {
                switch (SDbParams.CurrentServerType) {
                    case EServerType.MySql:
                        WipeTablesAndViews();
                        break;
                    case EServerType.MsSql:
                        aggregateObjects.ForEach(dobj => dobj.Drop());
                        domainObjects.ForEach(dobj => dobj.Drop());
                        break;
                    default:
                        throw new ArgumentException("Unsupported Server Type");
                }
            }

            domainObjects.ForEach(dobj => dobj.BuildTable());
            domainObjects.ForEach(dobj => dobj.BuildConstraints());
            aggregateObjects.ForEach(aggobj => aggobj.BuildView());

        }


        public static Type ParseType(AssemblyName assemblyName, string slug) {

            var assembly = Assembly.Load(assemblyName);
            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.GetInterfaces().Any(inf => inf.Equals(typeof(IDOBase)))) {
                        if ((Activator.CreateInstance(t) as IDOBase).Slug == slug) {
                            return t;
                        }
                    } else if (t.GetInterfaces().Any(inf => inf.Equals(typeof(IAGBase)))) {
                        if ((Activator.CreateInstance(t) as IAGBase).Slug == slug) {
                            return t;
                        }
                    }
                } catch {
                    continue;
                }
            }
            return null;

        }

        private static void GenerateObjects(AssemblyName assemblyName, out List<IDOBase> domainObjects, out List<IAGBase> aggregateObjects) {
            var assembly = Assembly.Load(assemblyName);
            domainObjects = new List<IDOBase>();
            aggregateObjects = new List<IAGBase>();
            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.IsSubclassOf(typeof(DOBase<>).MakeGenericType(t))) {
                        domainObjects.Add(Activator.CreateInstance(t) as IDOBase);
                    }
                } catch {
                    continue;
                }
            }

            foreach (var t in assembly.GetTypes()) {
                try {
                    if (t.IsSubclassOf(typeof(AGBase<>).MakeGenericType(t))) {
                        aggregateObjects.Add(Activator.CreateInstance(t) as IAGBase);
                    }
                } catch {
                    continue;
                }
            }
        }

        private static void WipeTablesAndViews() {
            var script = "";
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    script = @"
SET FOREIGN_KEY_CHECKS = 0;
SET GROUP_CONCAT_MAX_LEN=32768;
SET @views = NULL;
SELECT GROUP_CONCAT('`', TABLE_NAME, '`') INTO @views
  FROM information_schema.views
  WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@views,'dummy') INTO @views;

SET @views = CONCAT('DROP VIEW IF EXISTS ', @views);
PREPARE stmt FROM @views;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @tables = NULL;
SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables
  FROM information_schema.tables
  WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@tables,'dummy') INTO @tables;

SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
PREPARE stmt2 FROM @tables;
EXECUTE stmt2;
DEALLOCATE PREPARE stmt2;
SET FOREIGN_KEY_CHECKS = 1;
".Puts(SDbParams.CurrentServerType);
                    break;
                case EServerType.MsSql:
                    throw new InvalidOperationException("MSSQL Not Supported For wiping All Tables At Once");
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(script)) {
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
