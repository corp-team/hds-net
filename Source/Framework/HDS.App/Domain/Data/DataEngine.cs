﻿using HDS.App.Extensions.Static;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace HDS.App.Domain.Data {

    internal class DataEngine : IDataEngine {

        private IDbCommand _Command;
        private IDbCommand Command {
            get {
                if (_Command == null) {
                    switch (SDbParams.CurrentServerType) {
                        case EServerType.MySql:
                            _Command = new MySqlCommand();
                            break;
                        case EServerType.MsSql:
                            _Command = new SqlCommand();
                            break;
                        default:
                            throw new NotImplementedException("Unsupported option");
                    }
                }
                return _Command;
            }
        }

        public IDbCommand ConnectifiedCommand(string commandText = "") {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var mysqlConnection = new MySqlConnection(SDbParams.ConnectionString());
                    if (!String.IsNullOrEmpty(commandText)) {
                        Command.CommandText = commandText;
                    }
                    Command.Connection = mysqlConnection;
                    Command.Connection.Open();
                    return Command;
                case EServerType.MsSql:
                    var mssqlConnection = new SqlConnection(SDbParams.ConnectionString());
                    if (!String.IsNullOrEmpty(commandText)) {
                        Command.CommandText = commandText;
                    }
                    Command.Connection = mssqlConnection;
                    Command.Connection.Open();
                    return Command;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }
        public IDbCommand IdentityCommand(IDbCommand parentCommand) {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    Command.CommandText = "SELECT LAST_INSERT_ID()";
                    Command.Connection = parentCommand.Connection as MySqlConnection;
                    return Command;
                case EServerType.MsSql:
                    Command.CommandText = "SELECT @@IDENTITY";
                    Command.Connection = parentCommand.Connection as SqlConnection;
                    return Command;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

        public IDataEngine AddWithValue(string name, object value) {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var mysqlcommand = Command as MySqlCommand;
                    if (mysqlcommand.Parameters.Contains(name)) {
                        if (value.HasValue()) {
                            mysqlcommand.Parameters[name].Value = value;
                        }
                        return this;
                    }
                    if (value.GetType().Equals(typeof(DateTime))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        prm.Value = value;
                        mysqlcommand.Parameters.Add(prm);
                    } else if (value.GetType().Equals(typeof(Guid))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.Guid
                        };
                        prm.Value = value;
                        mysqlcommand.Parameters.Add(prm);
                    } else {
                        mysqlcommand.Parameters.AddWithValue(name, value);
                    }
                    return this;
                case EServerType.MsSql:
                    var sqlcommand = Command as SqlCommand;
                    if (sqlcommand.Parameters.Contains(name)) {
                        if (value.HasValue()) {
                            sqlcommand.Parameters[name].Value = value;
                        }
                        return this;
                    }
                    if (value.GetType().Equals(typeof(DateTime))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.DateTime2
                        };
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else if (value.GetType().Equals(typeof(Guid))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.UniqueIdentifier
                        };
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else {
                        sqlcommand.Parameters.AddWithValue(name, value);
                    }
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public IDataEngine AddParameter(string name, Type type) {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var mysqlcommand = Command as MySqlCommand;
                    if (type.Equals(typeof(DateTime))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.DateTime
                        };
                        mysqlcommand.Parameters.Add(prm);
                    } else if (type.Equals(typeof(Guid))) {
                        var prm = new MySqlParameter {
                            ParameterName = name,
                            MySqlDbType = MySqlDbType.Guid
                        };
                        mysqlcommand.Parameters.Add(prm);
                    } else {
                        var prm = new MySqlParameter {
                            ParameterName = name
                        };
                        mysqlcommand.Parameters.Add(prm);
                    }
                    return this;
                case EServerType.MsSql:
                    var sqlcommand = Command as SqlCommand;
                    if (type.Equals(typeof(DateTime))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.DateTime2
                        };
                        sqlcommand.Parameters.Add(prm);
                    } else if (type.Equals(typeof(Guid))) {
                        var prm = new SqlParameter {
                            ParameterName = name,
                            SqlDbType = SqlDbType.UniqueIdentifier
                        };
                        sqlcommand.Parameters.Add(prm);
                    } else {
                        var prm = new SqlParameter {
                            ParameterName = name,
                        };
                        sqlcommand.Parameters.Add(prm);
                    }
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public void Dispose() {
            if (Command.Connection.State == ConnectionState.Open) {
                Command.Connection.Close();
            }
        }

        public async Task<int> RunCommand(IDbCommand command) {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return await (command as MySqlCommand).ExecuteNonQueryAsync();
                case EServerType.MsSql:
                    return await (command as SqlCommand).ExecuteNonQueryAsync();
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
        }

        public async Task<DbDataReader> ReadCommand(IDbCommand command) {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    try {
                        return await (command as MySqlCommand).ExecuteReaderAsync(CommandBehavior.CloseConnection);
                    } catch (MySqlException ex) {
                        throw ex;
                    }
                case EServerType.MsSql:
                    try {
                        return await (command as SqlCommand).ExecuteReaderAsync(CommandBehavior.CloseConnection);
                    } catch (SqlException ex) {
                        throw ex;
                    }
                default:
                    throw new ArgumentException($"Server Key: {SDbParams.CurrentServerType} Not Implemented");
            }
        }
    }

}
