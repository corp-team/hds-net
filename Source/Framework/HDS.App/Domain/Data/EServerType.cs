﻿namespace HDS.App.Domain.Data {

    public enum EServerType {

        Unset = 1,
        MsSql = 2,
        MySql = 3,

    }

}
