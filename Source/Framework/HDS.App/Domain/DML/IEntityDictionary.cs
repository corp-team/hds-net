﻿
using System;
using System.Collections.Generic;

namespace HDS.App.Domain.DML {

    public interface IEntityDictionary {

        List<KeyValuePair<Type, object>> Dict { get; }

    }

}