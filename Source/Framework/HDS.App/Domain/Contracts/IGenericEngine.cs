﻿using HDS.App.Domain.Aggregate;
using System;
using System.Threading.Tasks;
using System.Reflection;

namespace HDS.App.Domain.Contracts {
    public interface IGenericEngine {
        Task<IDMLResponse> GenericSelectAll();
        Task<IDMLResponse> GenericSelectAll(Func<dynamic, bool> filter = null);
        Task<IDMLResponse> GenericSelectByID(long id);

        IDMLResponse GenericSelectAllSync();
        IDMLResponse GenericSelectByIDSync(long id);
        Task<IDMLResponse> GenericDelete(long id);
        Task<IDMLResponse> GenericUpdateAggregate(IAGBase aggregate);
        IDMLResponse GenericSelectByComparison<T>(PropertyInfo targetProp, T target);
    }

}
