﻿
using System;
using System.Linq.Expressions;

namespace HDS.App.Domain.Objects {

    public interface IDOConstraintBuilder<TEntity> where TEntity : DOBase<TEntity> {

        DORelationBuilder ForeignKey<TKey>(params Expression<Func<TEntity, TKey>>[] selectors);

    }

}