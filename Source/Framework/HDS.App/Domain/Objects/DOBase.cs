﻿
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using HDS.App.Engines;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HDS.App.Domain.Objects {

    public abstract class DOBase<TEntity> : BindableBase, IDOBase
        where TEntity : DOBase<TEntity> {

        protected abstract void Map(IDOTableBuilder<TEntity> builder);

        public abstract string TextValue { get; }

        public DOBase() {

        }

        private IDOTableBuilder<TEntity> _DOBuilder;

        protected IDOTableBuilder<TEntity> TableBuilder {
            get {
                if (_DOBuilder == null) {
                    _DOBuilder = SDbParams.TableBuilder<TEntity>();
                    Map(TableBuilder);
                }
                return _DOBuilder;
            }
        }

        public int? GetMaxLengthOf(MemberInfo key) {
            return TableBuilder.PropBuilders().SingleOrDefault(pb => pb.Prop.Name.Equals(key.Name))?.MaxLength;
        }

        public IEnumerable<(MemberInfo foreign, MemberInfo referencing)> GetKeys() {
            foreach (var item in TableBuilder.RelationKeys()
                .Zip(TableBuilder.ReferencingKeys(), (rlk, rfk) => (foreign: rlk, referencing: rfk))) {
                yield return item;
            }
        }

        public IEnumerable<MemberInfo> GetReferencingKeys() {
            return TableBuilder.ReferencingKeys();
        }

        public IEnumerable<MemberInfo> GetRelationKeys() {
            return TableBuilder.RelationKeys();
        }
        public IDOSchemaBuilder SchemaBuilder {
            get {
                return TableBuilder.SchemaBuilder;
            }
        }

        public long ID { get; set; }

        public string Slug {
            get {
                return SchemaBuilder.GetTableName().ToLower();
            }
        }

        public IDOBase BuildConstraints() {

            TableBuilder.BuildConstraints();
            return this;

        }

        public void BuildTable() {

            TableBuilder.BuildTable();

        }

        public IEnumerable<DOPropBuilder> TakeBuilders() {
            return TableBuilder.PropBuilders();
        }

        public IEnumerable<MemberInfo> TakeFields(EResolveBy by) {

            switch (by) {
                case EResolveBy.All:
                    return TableBuilder.ConstraintFields;
                case EResolveBy.AllAssigned:
                    return TableBuilder.ConstraintFields.Where(f => f.IsAssigned(this));
                case EResolveBy.AllRequired:
                    return TableBuilder.RequiredFields();
                case EResolveBy.AllPKs:
                    return TableBuilder.PKFields();
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public IDOBase Drop() {
            TableBuilder.CutOff();
            return this;
        }

        public void Absorb(TEntity other) {

            foreach (var field in TakeFields(EResolveBy.All)) {
                if (field.IsAssigned(other)) {
                    field.SetValue(this, field.GetValue(other));
                }
            }

        }

        public override string ToString() {
            return this.TextValue;
        }

        public IDMLResponse InsertSelf() {
            return SDataEngine.GenerateDOEngine<TEntity>().InsertSync(this as TEntity) as IDMLResponse;
        }

        public IDMLResponse DeleteSelf() {
            return SDataEngine.GenerateDOEngine<TEntity>().DeleteSync(ID) as IDMLResponse;
        }

    }

}
