﻿
namespace HDS.App.Domain.Objects {

    public interface ILineBuilder {

        string Build();

    }

}
