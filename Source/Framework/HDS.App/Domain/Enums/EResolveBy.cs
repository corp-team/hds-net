﻿namespace HDS.App.Domain.Enums {
    public enum EResolveBy {
        AllAssigned,
        All,
        AllRequired,
        AllPKs
    }
}
