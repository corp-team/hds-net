﻿
using HDS.App.Domain.Objects;
using System;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public interface ICursorExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        Task ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        Task ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);

    }

}