﻿
using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HDS.App.Domain.Query {

    public interface IQueryBuilder {

        IQueryBuilder AddField(MemberInfo prop);
        IQueryBuilder RemoveField<TEntity>(MemberInfo prop)
            where TEntity : DOBase<TEntity>;
        IQueryBuilder AndConstraint(MemberInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrConstraint(MemberInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrderByField(MemberInfo prop, EOrderBy orderBy);

        StringBuilder BuildQuery();
        List<MemberInfo> Fields { get; }
        IDataEngine Engine { get; }
    }

}