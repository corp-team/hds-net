﻿
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HDS.App.Domain.Aggregate {

    public interface IAGBase : IDomainBaseModel {

        long IDProperty { get; set; }
        IAGBase Drop();
        void BuildView();
        IAGSchemaBuilder GetSchemaBuilder();
        TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain>;
        IDOBase GetMappedDomainByType(Type domainType);
        bool IsColumnRequired(PropertyInfo property);
        IEnumerable<(MemberInfo domain, MemberInfo aggragate)> Mappings { get; }
        Type MainDomain { get; }
        Type[] ReferencedDomains { get; }
        string TextValue { get; }
        string ResourceName { get; }

        long GetMaxLength(MemberInfo key);
        IDMLResponse InsertSelf(bool commitMainDomain = true);
        IDMLResponse DeleteSelf();
    }

}