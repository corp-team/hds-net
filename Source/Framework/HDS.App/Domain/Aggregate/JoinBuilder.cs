﻿
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Linq.Expressions;

namespace HDS.App.Domain.Aggregate {

    public class JoinBuilder<TEntity, TAggregate> : IJoinBuilder<TEntity, TAggregate>
        where TEntity : DOBase<TEntity>
        where TAggregate : AGBase<TAggregate> {

        private readonly bool outerJoin;
        private readonly IAGSelectedViewBuilder<TAggregate> viewBuilder;

        public JoinBuilder(IAGSelectedViewBuilder<TAggregate> viewBuilder, bool outerJoin = false) {
            this.outerJoin = outerJoin;
            this.viewBuilder = viewBuilder;
        }

        public IAGSelectedViewBuilder<TAggregate> On<TOther>(Expression<Func<TEntity, TOther, bool>> selector) where TOther : DOBase<TOther> {

            var entity = Activator.CreateInstance<TEntity>() as IDOBase;
            var comparer = selector.Body as BinaryExpression;
            var leftEntity = Activator.CreateInstance(selector.Parameters[0].Type) as IDOBase;
            var rightEntity = Activator.CreateInstance(selector.Parameters[1].Type) as IDOBase;
            var left = comparer.Left.ExposeType().Equals(selector.Parameters[0].Type) ? comparer.Left : comparer.Right;
            var right = comparer.Right.ExposeType().Equals(selector.Parameters[1].Type) ? comparer.Right : comparer.Left;
            (this.viewBuilder as AGViewBuilder<TAggregate>).AppendJoinString("{0} JOIN {1} ON {2}".Puts(
                outerJoin ? "LEFT OUTER" : "INNER",
                entity.SchemaBuilder.GetFormatted(),
                "{0}.{1}{4}{2}.{3}".Puts(
                    leftEntity.SchemaBuilder.GetFormatted(), left.ExposeMember().Name,
                    rightEntity.SchemaBuilder.GetFormatted(), right.ExposeMember().Name,
                    comparer.NodeTypeString()
                ))
            );
            return this.viewBuilder;

        }
    }

}