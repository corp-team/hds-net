﻿using HDS.App.Api.Common;
using HDS.App.Domain.Objects;
using HDS.App.Engines;
using HDS.App.Extensions.Static;
using ElKitap.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using HDS.App.Api.Membership.Models;
using HDS.App.Api.Membership.Views;
using System.Globalization;
using HDS.App.Domain.Contracts;

namespace HDS.App.Tests.UnitTests {

    [TestClass]
    public class DataEngineTests {

        protected async Task<TResponse> ParseResponse<TResponse>(IHttpActionResult result) {
            var response = await result.ExecuteAsync(new CancellationToken());
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        [TestInitialize]
        public void BeforeTest() {
            Builder.Bootstrap();
        }

        [TestMethod]
        public void BuildsDatabase() {
            Builder.RebuildDomain();
        }

        [TestMethod]
        public async Task MappedDomainFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            var kunye = azaView.GetMappedDomain<Kunye>();
            var aza = azaView.GetMappedDomain<Aza>();
            Assert.IsNotNull(kunye);
            Assert.IsNotNull(aza);
            Assert.AreNotEqual(0, kunye.ID);
            Assert.AreNotEqual(0, aza.ID);
        }


        [TestMethod]
        public async Task ControllerInsertsView() {
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var session = new Session() { UserName = "25871077074", Password = "3132" };

            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);

            var api = new GenericController(typeof(AzaView));
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            var aggResponse = await ParseResponse<DMLResponse<Aza>>(await api.InsertAggregate(azaView));
            Assert.IsTrue(aggResponse.ContainsItem, aggResponse.Message);
        }

        [TestMethod]
        public async Task ControllerDeletesView() {
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaView = new AzaView {
                AzaTCKN= long.Parse($"4043{7.RandomDigits()}"),
                AzaSifre = "3132",
                KunyeIsim = $"Dilek-{1.RandomWords()}",
                KunyeSoyisim = $"{2.RandomWords()}",
                KunyeMemleket = 3.RandomWords(),
                KunyeMeslek = EMeslek.EvHanimi,
                KunyeMevlid = DateTime.Parse("31.10.1981", CultureInfo.GetCultureInfo("tr-TR"))
            };
            var api = new GenericController(typeof(AzaView));
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            var aggResponse = await ParseResponse<DMLResponse<Aza>>(await api.InsertAggregate(azaView));
            Assert.IsTrue(aggResponse.ContainsItem, aggResponse.Message);
            var deleteResponse = await ParseResponse<DMLResponse<Aza>>(await api.Delete(aggResponse.EntityID));
            Assert.IsTrue(deleteResponse.ContainsItem, deleteResponse.Message);
        }

        [TestMethod]
        public async Task ControllerEditsView() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);

            var api = new GenericController(typeof(AzaView));
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            azaView.KunyeIsim = 2.RandomWords();
            var aggResponse = await ParseResponse<DMLResponse<AzaView>>(await api.Edit(azaView));
            Assert.IsTrue(aggResponse.ContainsItem, aggResponse.Message);
            var updatedResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64("25871077074")));
            Assert.IsTrue(updatedResponse.ContainsItem, updatedResponse.Message);
            Assert.AreEqual(azaView.KunyeIsim, updatedResponse.SingleResponse.KunyeIsim);
            var allResponse = (await eng.SelectAll());
            Assert.IsTrue(allResponse.ContainsItem, allResponse.Message);
            Assert.AreEqual(1, allResponse.CollectionResponse.Count(av => av.KunyeIsim == azaView.KunyeIsim), azaView.KunyeIsim);

        }


        [TestMethod]
        public async Task ColumnRequiredOrNot() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            Assert.IsTrue(azaView.IsColumnRequired(new AzaView().ResolveProperties().Single(p => p.Name == "AzaTCKN")));
        }

        [TestMethod]
        public async Task DomainReferenceKeysFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            var aza = azaView.GetMappedDomain<Aza>();
            var kunyeReference = aza.GetRelationKeys().FirstOrDefault();
            Assert.IsNotNull(kunyeReference);
            Assert.AreEqual("KunyeID", kunyeReference.Name);
        }

        [TestMethod]
        public async Task OneFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var azaResponse = (await eng.SelectSingle(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem);
            Assert.IsNotNull(azaResponse.SingleResponse);
        }

        [TestMethod]
        public async Task OneFetchedByID() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var aza = (await SDataEngine.GenerateDOEngine<Aza>().SelectSingle(a => a.TCKN == Convert.ToInt64(session.UserName) && a.Sifre == session.Password))
                .SingleResponse;
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var existing = (await eng.SelectByID(aza.ID)).SingleResponse;
            Assert.IsNotNull(existing);
        }

        [TestMethod]
        public async Task OneOfManyFetchedBy() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var existing = (await eng.SelectBy(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password)).CollectionResponse.First();
            Assert.IsNotNull(existing);
        }

        [TestMethod]
        public async Task OneInserts() {
            await NewUserWith("25871077074", "3132");
        }

        [TestMethod]
        public async Task BuildsAll() {
            Builder.RebuildDomain();
            var mudur = await NewUserWith("25871077074", "3132");
            foreach (var i in Enumerable.Range(0, 100)) {
                await NewUserWith(11.RandomDigits(), 4.RandomDigits());
                var stokResp = NewKutuphaneItemWith(mudur, 2.RandomWords(), 1.RandomWords(), 1.RandomWords());
                Assert.IsTrue(stokResp.ContainsItem, stokResp.Message);
                Assert.AreNotEqual(0, stokResp.EntityID, stokResp.Message);
            }
        }

        [TestMethod]
        public async Task OneUpdated() {
            var session = new Session() { UserName = "25871077074", Password = "3132", Token = new ApiToken() { AccessToken = Guid.NewGuid().ToString("N") } };

            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var response = (await eng.SelectSingle(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password));
            Assert.IsNotNull(response.SingleResponse);

            var updated = await eng.Update(response.SingleResponse.ID, (modifier) => modifier
                .Update(a => a.AccessToken).Set(session.Token.AccessToken)
                .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11))
                .UpdateThen<Kunye, String>(obj => obj.Isim).Set("Hüseyn").FromWhere<Aza>((a, b) => a.ID == b.KunyeID)
                );
            Assert.IsTrue(updated.ContainsItem);
            Assert.AreEqual(session.Token.AccessToken, updated.SingleResponse.AccessToken);
            Assert.AreEqual("Hüseyn", SDataEngine.GenerateDOEngine<Kunye>().SelectSingleByIDSync(updated.SingleResponse.KunyeID).SingleResponse.Isim);
        }

        [TestMethod]
        public async Task OneUpsertedAggregate() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.ContainsItem, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            azaView.AzaActivationToken = "42342342980348309248023492";
            var dmlAgg = await SDataEngine.GenerateDOEngine<Aza>().UpdateAggregate(azaView);
            Assert.IsTrue(dmlAgg.ContainsItem, dmlAgg.Message);
            var token =
                (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password)).SingleResponse.AzaActivationToken;
            Assert.AreEqual(azaView.AzaActivationToken, token);
        }

        [TestMethod]
        public void InsertedIfAbsentSync() {
            var kunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().InsertIfAbsentSync(new Kunye() { IrtibatTelefonu = "5360331719", Isim = "Hüseyin", Memleket = "Sivas", Meslek = EMeslek.Hekim, Mevlid = DateTime.Parse("30.03.1981"), Soyisim = "Sönmez" }, k => k.ID);
            var aza = new Aza() { Sifre = "3132", TCKN = Convert.ToInt64("25871077074") };
            Assert.IsTrue(kunyeResponse.ContainsItem, kunyeResponse.Message);
            if (kunyeResponse.ContainsItem) {
                var kunye = kunyeResponse.SingleResponse;
                aza.KunyeID = kunye.ID;
                var azaResponse = SDataEngine.GenerateDOEngine<Aza>().InsertIfAbsentSync(aza, a => a.TCKN);
                Assert.IsTrue(azaResponse.ContainsItem, azaResponse.Message);
                if (azaResponse.ContainsItem) {
                    aza = azaResponse.SingleResponse;
                }
                //var deletedAzaResponse = SDataEngine.GenerateDOEngine<Aza>().DeleteSync(aza.ID);
                //Assert.IsTrue(deletedAzaResponse.Success, deletedAzaResponse.Message);
                //var deletedKunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().DeleteSync(kunye.ID);
                //Assert.IsTrue(deletedKunyeResponse.Success, deletedKunyeResponse.Message);
            }
        }

        private static async Task<Aza> NewUserWith(string userName, string password) {
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var session = new Session() { UserName = userName, Password = password };
            var kunyeResp = (await SDataEngine.GenerateDOEngine<Kunye>().Insert(
                new Kunye {
                    IrtibatTelefonu = 11.RandomDigits(),
                    Isim = 1.RandomWords(),
                    Soyisim = 1.RandomWords(),
                    Meslek = EMeslek.Alim,
                    Memleket = "Sivas",
                    Mevlid = DateTime.Parse("30.3.1981", CultureInfo.GetCultureInfo("tr-TR"))
                }));
            var kunye = kunyeResp.SingleResponse;
            var azaResp = (await SDataEngine.GenerateDOEngine<Aza>().Insert(new Aza { KunyeID = kunye.ID, Sifre = session.Password, TCKN = Convert.ToInt64(session.UserName) }));

            var aza = azaResp.SingleResponse;
            var emirVazifesi = (await SDataEngine.GenerateDOEngine<Vazife>().InsertIfAbsent(
                new Vazife {
                    Tarif = "Emir"
                }, v => v.Tarif)).SingleResponse;
            var azaVazifesi = (await SDataEngine.GenerateDOEngine<Vazife>().InsertIfAbsent(
                new Vazife {
                    Tarif = "Aza"
                }, v => v.Tarif)).SingleResponse;
            foreach (var tarif in new[] { "lists", "inserts", "edits", "deletes" }) {
                var kaide = (await SDataEngine.GenerateDOEngine<Kaide>().InsertIfAbsent(
                new Kaide {
                    Tarif = tarif,
                    Unsur = "aza"
                }, k => k.Tarif)).SingleResponse;
                await SDataEngine.GenerateDOEngine<Tahdit>().InsertIfAbsent(
                new Tahdit {
                    Tarif = $"{azaVazifesi.Tarif} {tarif}",
                    AzaID = aza.ID,
                    VazifeID = azaVazifesi.ID,
                    KaideID = kaide.ID
                }, k => k.Tarif);
            }
            return aza;
        }

        private IDMLResponse NewKutuphaneItemWith(Aza mudur, string stokTarif, string kutuphaneTarif, string kitapTarif) {

            var kutuphaneView = new KutuphaneView {
                KutuphaneMudurID = mudur.ID,
                StokTarif = stokTarif,
                StokMevcutAdet = long.Parse("1" + 3.RandomDigits()),
                StokSiparisAdet = long.Parse("1" + 2.RandomDigits()),
                KutuphaneTarif = kutuphaneTarif,
                KutuphaneIrtibatTelefonu = $"053{8.RandomDigits()}",
                KutuphaneIsAdresi = 4.RandomWords(),
                KutuphaneWebAdresi = $"http://{5.RandomDigits()}.el-kitap.com",
                KitapFiyat = 14.0,
                KitapTarif = kitapTarif,
                KitapYazar = 2.RandomWords(),
                BabTarif = "Bab-" + 10.RandomDigits(),
                MevzuTarif = "Mevzu-" + 3.RandomWords()
            };
            var resp = kutuphaneView.InsertSelf();
            return resp;
        }


    }

}
