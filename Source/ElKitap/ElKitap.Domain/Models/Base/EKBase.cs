﻿using HDS.App.Domain.Objects;

namespace ElKitap.Domain.Models.Base {

    public abstract class EKBase<TEntity> : DOBase<TEntity>
        where TEntity : DOBase<TEntity> {

        protected sealed override void Map(IDOTableBuilder<TEntity> builder) {
            builder.For(d => d.ID).IsTypeOf(EDataType.BigInt).IsIdentity();
            builder.PrimaryKey(d => d.ID);
            EKMap(builder);
        }

        protected abstract void EKMap(IDOTableBuilder<TEntity> builder);

    }

}
