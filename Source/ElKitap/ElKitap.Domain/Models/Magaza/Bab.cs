﻿using ElKitap.Domain.Models.Base;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;

namespace ElKitap.Domain.Models.Magaza {

    public class Bab : EKBase<Bab> {

        public string Tarif { get; set; }

        public long? UstBabID { get; set; }

        public override string TextValue => Tarif;

        protected override void EKMap(IDOTableBuilder<Bab> builder) {

            builder.MapsTo(x => { x.SchemaName("MZ").TableName("Bablar"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(d => d.UstBabID).ReferencesSelf<Bab>(d => d.ID);

            builder.UniqueKey(b => b.Tarif);
        }

    }

}
