﻿using ElKitap.Domain.Models.Base;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;

namespace ElKitap.Domain.Models.Magaza {

    public class Kutuphane : EKBase<Kutuphane> {

        public long MudurID { get; set; }

        public string Tarif { get; set; }

        public string WebAdresi { get; set; }

        public string IsAdresi { get; set; }

        public string IrtibatTelefonu { get; set; }

        public override string TextValue => Tarif;

        protected override void EKMap(IDOTableBuilder<Kutuphane> builder) {

            builder.MapsTo(x => { x.SchemaName("MZ").TableName("Kutuphaneler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.WebAdresi).IsTypeOf(EDataType.String).HasMaxLength(255);
            builder.For(d => d.IsAdresi).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.IrtibatTelefonu).IsTypeOf(EDataType.String).HasMaxLength(32);
            
            builder.ForeignKey(d => d.MudurID).References<Aza>(d => d.ID);

            builder.UniqueKey(d => d.WebAdresi);
        }

    }

}
