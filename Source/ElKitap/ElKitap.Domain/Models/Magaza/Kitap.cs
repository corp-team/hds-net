﻿using ElKitap.Domain.Models.Base;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;

namespace ElKitap.Domain.Models.Magaza {

    public class Kitap : EKBase<Kitap> {

        public long KutuphaneID { get; set; }

        public long BabID { get; set; }

        public long MevzuID { get; set; }

        public string Tarif { get; set; }

        public string Yazar { get; set; }

        public string Matbaa { get; set; }

        public double Fiyat { get; set; }

        public override string TextValue => $"{Tarif}, {Yazar}";

        protected override void EKMap(IDOTableBuilder<Kitap> builder) {

            builder.MapsTo(x => { x.SchemaName("MZ").TableName("Kitaplar"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.Yazar).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.Matbaa).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.Fiyat).IsTypeOf(EDataType.Money).IsRequired();
            
            builder.ForeignKey(d => d.KutuphaneID).References<Kutuphane>(d => d.ID);
            builder.ForeignKey(d => d.BabID).References<Bab>(d => d.ID);
            builder.ForeignKey(d => d.MevzuID).References<Mevzu>(d => d.ID);

        }

    }

}
