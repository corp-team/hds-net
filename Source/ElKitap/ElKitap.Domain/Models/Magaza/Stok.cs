﻿using ElKitap.Domain.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;

namespace ElKitap.Domain.Models.Magaza {
    public class Stok : EKBase<Stok> {

        public long KitapID { get; set; }

        public string Tarif { get; set; }

        public long MevcutAdet { get; set; }

        public long SiparisAdet { get; set; }

        public override string TextValue => Tarif;

        protected override void EKMap(IDOTableBuilder<Stok> builder) {

            builder.MapsTo(x => { x.SchemaName("MZ").TableName("Stok"); });

            builder.For(s => s.Tarif).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
            builder.For(s => s.MevcutAdet).IsTypeOf(EDataType.BigInt).IsRequired();
            builder.For(s => s.SiparisAdet).IsTypeOf(EDataType.BigInt).IsRequired();

            builder.ForeignKey(s => s.KitapID).References<Kitap>(k => k.ID);

            builder.UniqueKey(s => s.Tarif);
            builder.UniqueKey(s => s.KitapID);

        }
    }
}
