﻿using ElKitap.Domain.Resources;
using HDS.App.Api.Membership.Models;
using HDS.App.Api.Static;
using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using HDS.App.Engines;
using System;
using System.Web.Hosting;

namespace ElKitap.Domain {
    public static class Builder {

        static Builder() {
            Bootstrap();
        }
        public static void Bootstrap() {
            SDbParams.SetConnectionString(DBConstants.LocalhostConnectionString, EConnectionStringMode.Debug, EServerType.MySql);
            SDbParams.SetConnectionString(DBConstants.RemoteConnectionString, EConnectionStringMode.Release, EServerType.MySql);
            SAPIValues.CurrentDebugHost      = "http://192.168.1.33:114";
            SAPIValues.CurrentProductionHost = "http://api.el-kitap.com";
            SAPIValues.CurrentRPIVersion     = "v1";
        }


        public static void RebuildDomain() {
            SDataEngine.BuildDomain(typeof(Aza).Assembly.GetName(), drop: true);
            SDataEngine.BuildDomain(typeof(Builder).Assembly.GetName(), drop: false);
        }

        internal static string CurrentConnectionString {
            get {
                return SDbParams.ConnectionString();
            }
        }

        internal static string CurrentSchema {
            get {
                return SDbParams.LocalMachine() ? DBConstants.LocalhostSchemaName : DBConstants.RemoteSchemaName;
            }
        }
    }
}
