﻿using System;
using HDS.App.Domain.Aggregate;
using HDS.App.Api.Membership.Models;
using HDS.App.Extensions.Decorators;
using ElKitap.Domain.Models.Magaza;
using System.Globalization;
using ElKitap.Domain.Views.Resources;
using HDS.App.Engines;
using HDS.App.Domain.Contracts;

namespace HDS.App.Api.Membership.Views {

    [ResourceLocator(typeof(KutuphaneResource))]
    public class KutuphaneView : AGBase<KutuphaneView> {

        private IDOEngine<Aza> _Engine = SDataEngine.GenerateDOEngine<Aza>();

        [IDPropertyLocator]
        public long KutuphaneID { get; set; }

        public long KutuphaneStokID { get; set; }

        [ViewBinder(typeof(AzaView), "MudurID")]
        public long KutuphaneMudurID { get; set; }

        [TableColumn(Order = 1)]
        [ComputedField(EComputedType.FormattedString, typeof(long))]
        public string KutuphaneMudurComputed {
            get {
                var resp = _Engine.SelectSingleByIDSync(KutuphaneMudurID);
                if (resp.ContainsItem) {
                    return resp.SingleResponse.TextValue;
                } else {
                    return "";
                }
            }
            set => KutuphaneMudurID = long.TryParse(value, out var val) ? val : 0;
        }

        [TableColumn(Order = 2)]
        public string StokTarif { get; set; }

        [TableColumn(Order = 3)]
        public string KutuphaneTarif { get; set; }

        [TableColumn(Order = 4)]
        public string KutuphaneWebAdresi { get; set; }

        [TableColumn(Order = 5)]
        public string KutuphaneIsAdresi { get; set; }

        [TableColumn(Order = 6)]
        public string KutuphaneIrtibatTelefonu { get; set; }

        [TableColumn(Order = 7)]
        public string KitapTarif { get; set; }

        [TableColumn(Order = 8)]
        public string KitapYazar { get; set; }

        public double KitapFiyat { get; set; }

        [TableColumn(Order = 9)]
        [ComputedField(EComputedType.FormattedString, typeof(double))]
        public string KitapFiyatComputed {
            get => String.Format("{0:C}", KitapFiyat, CultureInfo.DefaultThreadCurrentUICulture);
            set => KitapFiyat = double.Parse(value, NumberStyles.Currency | NumberStyles.AllowCurrencySymbol);
        }

        [TableColumn(Order = 10)]
        public long StokMevcutAdet { get; set; }

        [TableColumn(Order = 11)]
        public long StokSiparisAdet { get; set; }

        [TableColumn(Order = 12)]
        public string MevzuTarif { get; set; }

        [TableColumn(Order = 13)]
        public string BabTarif { get; set; }

        public override Type MainDomain => typeof(Kutuphane);

        public override Type[] ReferencedDomains => new[] { typeof(Aza), typeof(Mevzu), typeof(Bab), typeof(Kitap), typeof(Stok) };

        public override string TextValue => KutuphaneTarif;

        public override string ResourceName => "kutuphane";

        protected override void Map(IAGViewBuilder<KutuphaneView> builder) {
            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("KutuphaneView"))
                .Select<Aza>(li =>
                    li
                    .Map(x => x.ID, x => x.KutuphaneMudurID)
                ).OuterJoin<Kutuphane>(li =>
                    li
                    .Map(x => x.ID, x => x.KutuphaneID)
                    .Map(x => x.Tarif, x => x.KutuphaneTarif)
                    .Map(x => x.WebAdresi, x => x.KutuphaneWebAdresi)
                    .Map(x => x.IsAdresi, x => x.KutuphaneIsAdresi)
                    .Map(x => x.IrtibatTelefonu, x => x.KutuphaneIrtibatTelefonu)
                ).On<Aza>((a, b) => a.MudurID == b.ID)
                .OuterJoin<Kitap>(li =>
                    li
                    .Map(x => x.Tarif, x => x.KitapTarif)
                    .Map(x => x.Yazar, x => x.KitapYazar)
                    .Map(x => x.Fiyat, x => x.KitapFiyat)
                ).On<Kutuphane>((a, b) => a.KutuphaneID == b.ID)
                .OuterJoin<Stok>(li =>
                    li
                    .Map(x => x.ID, x => x.KutuphaneStokID)
                    .Map(x => x.MevcutAdet, x => x.StokMevcutAdet)
                    .Map(x => x.SiparisAdet, x => x.StokSiparisAdet)
                    .Map(x => x.Tarif, x => x.StokTarif)
                ).On<Kitap>((a, b) => a.KitapID == b.ID)
                .OuterJoin<Mevzu>(li =>
                    li.Map(x => x.Tarif, x => x.MevzuTarif)
                ).On<Kitap>((a, b) => a.ID == b.MevzuID)
                .OuterJoin<Bab>(li =>
                    li.Map(x => x.Tarif, x => x.BabTarif)
                ).On<Kitap>((a, b) => a.ID == b.BabID);
        }
    }
}