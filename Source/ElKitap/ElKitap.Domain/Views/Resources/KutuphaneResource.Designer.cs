﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ElKitap.Domain.Views.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class KutuphaneResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal KutuphaneResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ElKitap.Domain.Views.Resources.KutuphaneResource", typeof(KutuphaneResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Category.
        /// </summary>
        public static string BabTarif {
            get {
                return ResourceManager.GetString("BabTarif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID.
        /// </summary>
        public static string IDProperty {
            get {
                return ResourceManager.GetString("IDProperty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Book Price.
        /// </summary>
        public static string KitapFiyatComputed {
            get {
                return ResourceManager.GetString("KitapFiyatComputed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Book Name.
        /// </summary>
        public static string KitapTarif {
            get {
                return ResourceManager.GetString("KitapTarif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Book Author.
        /// </summary>
        public static string KitapYazar {
            get {
                return ResourceManager.GetString("KitapYazar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Phone.
        /// </summary>
        public static string KutuphaneIrtibatTelefonu {
            get {
                return ResourceManager.GetString("KutuphaneIrtibatTelefonu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        public static string KutuphaneIsAdresi {
            get {
                return ResourceManager.GetString("KutuphaneIsAdresi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Director.
        /// </summary>
        public static string KutuphaneMudurComputed {
            get {
                return ResourceManager.GetString("KutuphaneMudurComputed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string KutuphaneTarif {
            get {
                return ResourceManager.GetString("KutuphaneTarif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Web Address.
        /// </summary>
        public static string KutuphaneWebAdresi {
            get {
                return ResourceManager.GetString("KutuphaneWebAdresi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subject.
        /// </summary>
        public static string MevzuTarif {
            get {
                return ResourceManager.GetString("MevzuTarif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Amount.
        /// </summary>
        public static string StokMevcutAdet {
            get {
                return ResourceManager.GetString("StokMevcutAdet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Order Mount.
        /// </summary>
        public static string StokSiparisAdet {
            get {
                return ResourceManager.GetString("StokSiparisAdet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stock Name.
        /// </summary>
        public static string StokTarif {
            get {
                return ResourceManager.GetString("StokTarif", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Display Library.
        /// </summary>
        public static string TitleDisplayItem {
            get {
                return ResourceManager.GetString("TitleDisplayItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Library.
        /// </summary>
        public static string TitleEditItem {
            get {
                return ResourceManager.GetString("TitleEditItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Library.
        /// </summary>
        public static string TitleEntity {
            get {
                return ResourceManager.GetString("TitleEntity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add New Library.
        /// </summary>
        public static string TitleInsertItem {
            get {
                return ResourceManager.GetString("TitleInsertItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Libraries.
        /// </summary>
        public static string TitleItemsTable {
            get {
                return ResourceManager.GetString("TitleItemsTable", resourceCulture);
            }
        }
    }
}
