﻿using HDS.App.Api.Membership.Models;
using HDS.App.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HDS.App.Extensions.Decorators;
using HDS.App.Api.Decorators;
using System.Threading.Tasks;
using HDS.App.Engines;
using HDS.App.Api.Common;
using System.Web.Http.Cors;
using HDS.App.Extensions.Static;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Api.Membership.Views;

namespace ElKitap.Api.Controllers {

    [ApiConfig(key: "aza", controller: typeof(AzaController), aggregate: typeof(AzaView))]
#if DEBUG
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
#else
    [EnableCors(origins: "http://www.e-mektep.net", headers: "*", methods: "*")]
#endif
    public class AzaController : GenericController {

        public AzaController() : base(typeof(AzaView)) { }

        private Func<string, Session> FalseToken => (message) => {
            return new Session {
                Token = new ApiToken {
                    AccessToken = "",
                    Success = false
                },
                ClientMessage = message
            };
        };

        #region Session Actions
        [HQPost("jeton", ArgumentType = typeof(Session)), Musaadeli]
        public async Task<IHttpActionResult> Token([FromBody]Session session) {
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var existingResponse = (await eng.SelectSingle(a => a.TCKN == Convert.ToInt64(session.UserName) && a.Sifre == session.Password));
            if (existingResponse.ContainsItem) {
                var existing = existingResponse.SingleResponse;
                if (existing != null) {
                    if (DateTime.Now > existing.AccessTokenExpiresOn) {
                        session.Token.AccessToken = Guid.NewGuid().ToString("N");
                        session.Token.Success = true;
                        await eng.Update(existing.ID, (modifier) => modifier
                           .Update(a => a.AccessToken).Set(session.Token.AccessToken)
                           .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    } else {
                        session.Token.AccessToken = existing.AccessToken;
                        session.Token.Success = true;
                    }
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);
        }
        [HQPost("oturum-ac", ArgumentType = typeof(Session)), Musaadeli]
        public async Task<IHttpActionResult> Logon([FromBody]Session session) {

            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var token = session.Token.AccessToken;
            var existingResponse = (await eng.SelectSingle(a => a.AzaAccessToken == token));
            if (existingResponse.ContainsItem) {
                var aza = existingResponse.SingleResponse;
                if (aza != null) {
                    if (DateTime.Now > aza.AzaAccessTokenExpiresOn) {
                        session = FalseToken("Token Expired");
                    } else {
                        session.Token.AccessToken = aza.AzaAccessToken;
                        session.Token.Success = true;
                        session.ClientMessage = $"Welcome {aza}, Begin Navigating!";
                    }
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);

        }
        [HQPost("oturum-kapat", ArgumentType = typeof(Session))]
        public async Task<IHttpActionResult> Logoff([FromBody]Session session) {

            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var token = session.Token.AccessToken;
            var existingResponse = (await eng.SelectSingle(a => a.AzaAccessToken == token));
            if (existingResponse.ContainsItem) {
                var aza = existingResponse.SingleResponse;
                if (aza != null) {
                    session.Token.AccessToken = "";
                    session.Token.Success = true;
                    session.ClientMessage = $"Goodbye {aza}, See you later!";
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);

        }
        #endregion

    }
}
