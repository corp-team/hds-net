﻿using ElKitap.Domain.Models.Magaza;
using HDS.App.Api.ActionFilters;
using HDS.App.Api.Decorators;
using HDS.App.Api.GenericRouting;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Api.Membership.Models;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Tracing;

namespace ElKitap.Api {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {

            DataConfig.Register(config);
            SApiInjector.InjectApi<WebApiApplication, Stok>(config);
            EnableCorsAttribute cors = null;
            if (Assembly.GetExecutingAssembly().IsDebugBuild()) {
                cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-HDS-Tahdit");
            } else {
                cors = new EnableCorsAttribute(origins: "http://www.el-kitap.com", headers: "*", methods: "*", exposedHeaders: "X-HDS-Tahdit");
            }
            config.EnableCors();
            config.EnsureInitialized();

        }
    }
}
