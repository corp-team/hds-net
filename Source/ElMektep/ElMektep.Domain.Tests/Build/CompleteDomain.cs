﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElMektep.Domain;

namespace ElMektep.Domain.Tests {
    [TestClass]
    public class CompleteDomain {
        [TestMethod]
        public void BuildDatabaseWorksOnMySql() {
            Builder.RebuildDomain();
        }
    }
}
