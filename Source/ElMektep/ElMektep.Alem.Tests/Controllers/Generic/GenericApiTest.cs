﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HDS.App.Domain.Objects;
using HDS.App.Api.Contracts;
using System.Threading.Tasks;
using HDS.App.Domain.Aggregate;
using System.Collections.Generic;
using System.Linq;
using HDS.App.Extensions.Decorators;
using Newtonsoft.Json;
using System.Web.Http;
using System.Threading;
using System.Net.Http;
using HDS.App.Domain.Contracts;
using System.IO;
using ElMektep.Domain;
using HDS.App.Api.Common;

namespace ElMektep.Alem.Tests.Controllers.Generic {

    [TestClass]
    public abstract class GenericApiTest<TController, TDomain, TAggregate> : GenericRestTest<TController, TDomain, TAggregate>
        where TDomain : DOBase<TDomain>
        where TAggregate : AGBase<TAggregate>
        where TController : GenericController {

        protected async Task<TResponse> ParseResponse<TResponse>(IHttpActionResult result) {
            var response = await result.ExecuteAsync(new CancellationToken());
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        protected override abstract void TemplateEntityGenerator(out TAggregate domainObject);

        protected override abstract TAggregate EntityModifier(TDomain domainObject);

        [TestInitialize]
        public void BeforeTest() {
            Builder.Bootstrap();
            (ApiController as ApiController).ControllerContext.Configuration = new HttpConfiguration();
            (ApiController as ApiController).Request = new HttpRequestMessage();
        }

        [TestMethod]
        public async Task AllRecordsFetched() {
            var inserteds = new List<TDomain>();
            for (int i = 0; i < 10; i++) {
                TemplateEntityGenerator(out var templateEntity);
                inserteds.Add((await ParseResponse<DMLResponse<TDomain>>(await ApiController.InsertAggregate(templateEntity))).SingleResponse);
            }
            var result = await ParseResponse<DMLResponse<TAggregate>>(await ApiController.All());
            Assert.IsTrue(result.Success, result.Message);
            Assert.AreNotEqual(0, result.CollectionResponse.Count());
            result.CollectionResponse.ToList().ForEach(d => Assert.IsTrue(d.IDProperty != 0));
            //inserteds.ForEach(async ins => await ApiController.Delete(id: ins.ID));
        }

        [TestMethod]
        public async Task OneFetched() {
            TemplateEntityGenerator(out var one);
            var inserted = (await ParseResponse<DMLResponse<TDomain>>(await ApiController.InsertAggregate(one))).SingleResponse;
            var fetched = await ParseResponse<TAggregate>(await ApiController.One(id: inserted.ID));
            Assert.IsNotNull(fetched);
            Assert.AreNotEqual(0, fetched.IDProperty);
            var deleted = await ParseResponse<DMLResponse<TDomain>>(await ApiController.Delete(id: fetched.IDProperty));
            Assert.IsTrue(deleted.Success);
        }

        [TestMethod]
        public async Task OneRecordCRUD() {
            TemplateEntityGenerator(out var templateEntity);
            var inserted = await ParseResponse<DMLResponse<TDomain>>(await ApiController.InsertAggregate(templateEntity));
            Assert.IsNotNull(inserted.SingleResponse, inserted.Message);
            Assert.AreNotEqual(0, inserted.SingleResponse.ID);
            var aggregate = EntityModifier(inserted.SingleResponse);
            var modified = await ParseResponse<DMLResponse<dynamic>>(await ApiController.Edit(aggregate));
            Assert.IsTrue(modified.Success, modified.Message);
            Assert.AreEqual(inserted.SingleResponse.ID, Convert.ToInt64(modified.SingleResponse.IDProperty));
            //var deleted = await ParseResponse<DMLResponse<TDomain>>(await ApiController.Delete(id: modified.SingleResponse.ID));
            //Assert.IsTrue(deleted.Success);
        }

        [TestMethod]
        public async Task AllColumnsFetched() {
            var result = await ParseResponse<IEnumerable<IDictionary<string, object>>>(await ApiController.Columns());
            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());
            result.ToList().ForEach(col => Assert.IsTrue(col.Keys.Contains("name")));
        }

        [TestMethod]
        public async Task AllFieldsFetched() {
            var result = await ParseResponse<IEnumerable<IDictionary<string, object>>>(await ApiController.Fields((long)EFormType.Insert));
            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());
            result.ToList().ForEach(col => Assert.IsTrue(col.Keys.Contains("name")));
        }

    }
}
