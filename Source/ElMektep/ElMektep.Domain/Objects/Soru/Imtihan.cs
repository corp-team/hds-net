﻿using ElMektep.Domain.Objects.AkademikKadro;
using ElMektep.Domain.Objects.Core;
using ElMektep.Domain.Objects.Mufredat;
using HDS.App.Domain.Objects;
using System;

namespace ElMektep.Domain.Objects.Soru {

    public class Imtihan : EMBase<Imtihan> {

        public string Baslik { get; set; }

        public DateTime ImtihanTarihi { get; set; }

        public long OgrenciID { get; set; }

        public long DersID { get; set; }

        public override string TextValue => Baslik;

        protected override void EMMap(IDOTableBuilder<Imtihan> builder) {

            builder.MapsTo(x => { x.SchemaName("IM").TableName("Imtihanlar"); });
            builder.For(d => d.ImtihanTarihi).IsTypeOf(EDataType.DateTime).IsRequired();

            builder.ForeignKey(tc => tc.OgrenciID).References<Ogrenci>(s => s.ID);
            builder.ForeignKey(tc => tc.DersID).References<Ders>(s => s.ID);

            builder.UniqueKey(t => t.OgrenciID, t => t.DersID);

        }


    }
}
