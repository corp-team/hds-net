﻿using ElMektep.Domain.Objects.AkademikKadro;
using ElMektep.Domain.Objects.Core;
using ElMektep.Domain.Objects.Kurum;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class DersYuku : EMBase<DersYuku> {

        public string Baslik { get; set; }

        public long OgretimUyesiID { get; set; }

        public long ProgramID { get; set; }

        public override string TextValue => Baslik;

        protected override void EMMap(IDOTableBuilder<DersYuku> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersYuku"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(tc => tc.OgretimUyesiID).References<OgretimUyesi>(s => s.ID);
            builder.ForeignKey(tc => tc.ProgramID).References<Program>(s => s.ID);

        }

    }
}
