﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class Tedrisat : EMBase<Tedrisat> {

        public string Adi { get; set; }

        public long DersID { get; set; }

        public override string TextValue => Adi;

        protected override void EMMap(IDOTableBuilder<Tedrisat> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("Tedrisatlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.DersID).References<Ders>(x => x.ID);
        }

    }
}
