﻿using ElMektep.Domain.Objects.Core;
using ElMektep.Domain.Objects.Kurum;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class Mufredat : EMBase<Mufredat> {

        public string Adi { get; set; }

        public int YururlukYili { get; set; }

        public long ProgramID { get; set; }

        public override string TextValue => Adi;

        protected override void EMMap(IDOTableBuilder<Mufredat> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("Mufredatlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.YururlukYili).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(x => x.ProgramID).References<Program>(x => x.ID);

        }

    }
}
