﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.AkademikKadro {

    public class OgretimElemani : EMBase<OgretimElemani> {

        public string Ad { get; set; }

        public string Soyadi { get; set; }

        public long AzaID { get; set; }

        public override string TextValue => Ad;

        protected override void EMMap(IDOTableBuilder<OgretimElemani> builder) {

            builder.MapsTo(x => { x.SchemaName("AK").TableName("OgretimElemanlari"); });
            builder.For(d => d.Ad).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.Soyadi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.AzaID).References<Aza>(u => u.ID);
            builder.UniqueKey(x => x.AzaID);

        }

    }
}
