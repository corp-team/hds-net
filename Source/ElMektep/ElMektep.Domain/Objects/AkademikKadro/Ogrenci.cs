﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;

namespace ElMektep.Domain.Objects.AkademikKadro {

    public class Ogrenci : EMBase<Ogrenci> {

        public string OgrenciNo { get; set; }

        public int KayitYili { get; set; }

        public EKayitlanmaTipi KayitlanmaTipi { get; set; }

        public long AzaID { get; set; }

        public override string TextValue => OgrenciNo;

        protected override void EMMap(IDOTableBuilder<Ogrenci> builder) {

            builder.MapsTo(x => { x.SchemaName("AK").TableName("Ogrenciler"); });
            builder.For(d => d.OgrenciNo).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.KayitYili).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.KayitlanmaTipi).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.AzaID).References<Aza>(u => u.ID);
            builder.UniqueKey(x => x.AzaID);

        }

    }
}
