﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Kurum {

    public class Universite : EMBase<Universite> {

        public string Adi { get; set; }

        public string KurulusYeri { get; set; }

        public override string TextValue => Adi;

        protected override void EMMap(IDOTableBuilder<Universite> builder) {

            builder.MapsTo(x => { x.SchemaName("KR").TableName("Universiteler"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.KurulusYeri).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

        }

    }
}
