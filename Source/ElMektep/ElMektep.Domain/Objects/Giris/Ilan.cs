﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;

namespace ElMektep.Domain.Objects.Giris {

    public class Ilan : EMBase<Ilan> {

        public string Baslik { get; set; }

        public DateTime YayimTarihi { get; set; }

        public string OnYazi { get; set; }

        public string Etiketler { get; set; }

        public string BaslikResmi { get; set; }

        public EIlanTipi Tipi { get; set; }

        public override string TextValue => Baslik;

        protected override void EMMap(IDOTableBuilder<Ilan> builder) {

            builder.MapsTo(x => { x.SchemaName("GR").TableName("Ilanlar"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).HasMaxLength(128).IsRequired();
            builder.For(d => d.YayimTarihi).IsTypeOf(EDataType.Date);
            builder.For(d => d.OnYazi).IsTypeOf(EDataType.String).HasMaxLength(400).IsRequired();
            builder.For(d => d.Etiketler).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.BaslikResmi).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
            builder.For(d => d.Tipi).IsTypeOf(EDataType.Enum).IsRequired();

        }

    }
}
