﻿
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Giris {

    [ResourceLocator(typeof(Resources.IlanTipi))]
    public enum EIlanTipi {

        Tebligat = 1,
        Haber,
        Faaliyet

    }

}