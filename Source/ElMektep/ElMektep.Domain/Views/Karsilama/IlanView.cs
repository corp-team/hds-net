﻿using ElMektep.Domain.Objects.Giris;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;

namespace ElMektep.Domain.Views.Karsilama {

    [ResourceLocator(typeof(Resources.Ilan))]
    public class IlanView : AGBase<IlanView> {

        [IDPropertyLocator]
        public long DuyuruID { get; set; }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public string Baslik { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public DateTime YayimTarihi { get; set; }

        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string OnYazi { get; set; }

        [TableColumn(Order = 4), FormField(EFormType.Insert | EFormType.Edit)]
        public string Etiketler { get; set; }

        [TableColumn(Order = 5), FormField(EFormType.Insert | EFormType.Edit)]
        public string BaslikResmi { get; set; }

        public EIlanTipi IlanTipi { get; set; }
        [ComputedField(EComputedType.Enum, typeof(EIlanTipi))]
        [TableColumn(Order = 6), FormField(EFormType.Insert | EFormType.Edit)]
        public string AkademikDonemComputed {
            get {
                return IlanTipi.GetEnumResource();
            }
        }
        public override Type MainDomain => typeof(Ilan);

        protected override void Map(IAGViewBuilder<IlanView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("IlanView"))
                .Select<Ilan>(li =>
                    li
                    .Map(x => x.ID, x => x.DuyuruID)
                    .Map(x => x.Baslik, x => x.Baslik)
                    .Map(x => x.YayimTarihi, x => x.YayimTarihi)
                    .Map(x => x.OnYazi, x => x.OnYazi)
                    .Map(x => x.Etiketler, x => x.Etiketler)
                    .Map(x => x.BaslikResmi, x => x.BaslikResmi)
                    .Map(x => x.Tipi, x => x.IlanTipi)
                );

        }

    }

}
