﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ElMektep.Domain.Views.AkademikKadro.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AkademikTakvim {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AkademikTakvim() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ElMektep.Domain.Views.AkademikKadro.Resources.AkademikTakvim", typeof(AkademikTakvim).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Academic Term.
        /// </summary>
        public static string AkademikDonem {
            get {
                return ResourceManager.GetString("AkademikDonem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Begin Date.
        /// </summary>
        public static string Baslangic {
            get {
                return ResourceManager.GetString("Baslangic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End Date.
        /// </summary>
        public static string Bitis {
            get {
                return ResourceManager.GetString("Bitis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity.
        /// </summary>
        public static string Etkinlik {
            get {
                return ResourceManager.GetString("Etkinlik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Faculty.
        /// </summary>
        public static string FakulteAdi {
            get {
                return ResourceManager.GetString("FakulteAdi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to University.
        /// </summary>
        public static string UniversiteAdi {
            get {
                return ResourceManager.GetString("UniversiteAdi", resourceCulture);
            }
        }
    }
}
