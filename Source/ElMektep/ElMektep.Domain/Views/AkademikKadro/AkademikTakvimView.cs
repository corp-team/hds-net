﻿using ElMektep.Domain.Objects.Kurum;
using ElMektep.Domain.Objects.Mufredat;
using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;

namespace ElMektep.Domain.Views.AkademikKadro {

    [ResourceLocator(typeof(Resources.AkademikTakvim))]
    public class AkademikTakvimView : AGBase<AkademikTakvimView> {

        [IDPropertyLocator]
        public long AkademikTakvimID { get; set; }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public DateTime Baslangic { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public DateTime Bitis { get; set; }

        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string Etkinlik { get; set; }

        [TableColumn(Order = 4), FormField(EFormType.Insert | EFormType.Edit)]
        public string FakulteAdi { get; set; }

        [TableColumn(Order = 5), FormField(EFormType.Insert | EFormType.Edit)]
        public string UniversiteAdi { get; set; }

        public EAkademikDonem AkademikDonem { get; set; }
        [ComputedField(EComputedType.Enum, typeof(EAkademikDonem))]
        [TableColumn(Order = 6), FormField(EFormType.Insert | EFormType.Edit)]
        public string AkademikDonemComputed {
            get {
                return AkademikDonem.GetEnumResource();
            }
        }

        public override Type MainDomain => typeof(AkademikTakvim);

        protected override void Map(IAGViewBuilder<AkademikTakvimView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("AkademikTakvimView"))
                .Select<AkademikTakvim>(li =>
                    li
                    .Map(x => x.ID, x => x.AkademikTakvimID)
                    .Map(x => x.Baslangic, x => x.Baslangic)
                    .Map(x => x.Bitis, x => x.Bitis)
                    .Map(x => x.Aciklama, x => x.Etkinlik)
                    .Map(x => x.AkademikDonem, x => x.AkademikDonem)
                )
                .OuterJoin<Fakulte>(li => li.Map(x => x.Adi, x => x.FakulteAdi))
                    .On<AkademikTakvim>((fl, at) => fl.ID == at.FakulteID)
                .OuterJoin<Universite>(li => li.Map(x => x.Adi, x => x.UniversiteAdi))
                    .On<Fakulte>((uv, fl) => fl.UniversiteID == uv.ID);

        }

    }

}
