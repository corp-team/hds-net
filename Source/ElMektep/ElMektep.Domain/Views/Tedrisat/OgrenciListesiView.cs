﻿using ElMektep.Domain.Objects.AkademikKadro;
using ElMektep.Domain.Objects.Kurum;
using ElMektep.Domain.Objects.Mufredat;
using HDS.App.Api.Membership.Models;
using HDS.App.Domain.Aggregate;
using System;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;

namespace ElMektep.Domain.Views.Tedrisat {

    public class OgrenciListesiView : AGBase<OgrenciListesiView> {

        [IDPropertyLocator]
        public long DersYukuID { get; set; }

        public long DersID { get; set; }

        public long UyeID { get; set; }

        public long OgretimUyesiUyeID { get; set; }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public string OgrenciNo { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public string OgrenciAdiSoyadi { get; set; }

        public int OgrenciKayitYili { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(long))]
        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string TeorikSaatComputed {
            get {
                return Resources.DersListesiFormats.Saat.Puts(OgrenciKayitYili);
            }
        }
        public EKayitlanmaTipi OgrenciKayitTipi { get; set; }
        [ComputedField(EComputedType.Enum, typeof(EKayitlanmaTipi))]
        [TableColumn(Order = 4), FormField(EFormType.Insert | EFormType.Edit)]
        public string AkademikDonemComputed {
            get {
                return OgrenciKayitTipi.GetEnumResource();
            }
        }

        public override Type MainDomain => typeof(DersYuku);

        protected override void Map(IAGViewBuilder<OgrenciListesiView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("OgrenciListesiView"))
                .Select<DersYuku>(li => li.Map(x => x.ID, x => x.DersYukuID))
                .InnerJoin<Program>().On<DersYuku>((prg, dy) => prg.ID == dy.ProgramID)
                .InnerJoin<OgretimUyesi>(li => li
                        .Map(x => x.ID, x => x.OgretimUyesiUyeID)
                    ).On<DersYuku>((ogu, dy) => ogu.ID == dy.OgretimUyesiID)
                .InnerJoin<DersProgrami>().On<DersYuku>((dp, dy) => dp.DersYukuID == dy.ID)
                .InnerJoin<Ogrenci>(li => li
                        .Map(x => x.OgrenciNo, x => x.OgrenciNo)
                        .Map(x => x.KayitlanmaTipi, x => x.OgrenciKayitTipi)
                        .Map(x => x.KayitYili, x => x.OgrenciKayitYili)
                    ).On<DersProgrami>((og, dp) => og.ID == dp.OgrenciID)
                .InnerJoin<Aza>(li => li.Map(x => x.ID, x => x.UyeID)).On<Ogrenci>((uy, og) => uy.ID == og.AzaID)
                .InnerJoin<Kunye>(li => li.Map(x => x.Isim + " " + x.Soyisim, x => x.OgrenciAdiSoyadi)).On<Aza>((a, b) => a.ID == b.KunyeID)
                .InnerJoin<Mufredat>().On<Program>((mf, prg) => mf.ProgramID == prg.ID)
                .InnerJoin<Donem>().On<Mufredat>((dn, mf) => dn.MufredatID == mf.ID)
                .InnerJoin<Ders>(li =>
                    li.Map(d => d.ID, x => x.DersID))
                    .On<Donem>((drs, dn) => drs.DonemID == dn.ID);

        }

    }

}
