﻿using HDS.App.Api.Decorators;
using HDS.App.Api.Membership.Decorators;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace ElMektep.Alem {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            // Web API configuration and services
            config.Filters.Add(new TahditliAttribute());
            config.Filters.Add(new KaideAttribute());

            config.EnableCors();

            if (Assembly.GetExecutingAssembly().IsDebugBuild()) {
                var traceWriter = config.EnableSystemDiagnosticsTracing();
                traceWriter.IsVerbose = true;
                traceWriter.MinimumLevel = TraceLevel.Debug;
            }

            // Web API routes
            config.MapHttpAttributeRoutes();


        }
    }
}
